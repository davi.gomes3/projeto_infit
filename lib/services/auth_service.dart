import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';

class AutenticarException implements Exception {
  String mensagem;
  AutenticarException(this.mensagem);
}

class AuthService extends ChangeNotifier { // notificar todas as telas de houve ou nao autenticação

  FirebaseAuth _auth = FirebaseAuth.instance;
  User? usuario; // variável que permite valor nulo para verificar autenticação
  bool carregando = true; // propriedade de carregamento

  AuthService() {
    _authCheck();
  }

  _authCheck() {
    _auth.authStateChanges().listen((User? user) { // user poder esta nulo ou com as propriedades do usuário
      usuario = (user == null) ? null : user;
      carregando = false;
      notifyListeners(); // notificar que a propriedade usuário foi alterada
    });
  }

  _getUsuario() {
    usuario = _auth.currentUser;
    notifyListeners();
  }

  cadastrar(String email, String senha) async {
    try { // try catch para o email
      await _auth.createUserWithEmailAndPassword(email: email, password: senha);
      _getUsuario();
    }
    on FirebaseAuthException catch (e) {
      if (e.code == 'weak-passowrd') {
        throw AutenticarException("A senha é muito fraca");
      }
      else if (e.code == 'email-already-in-use') {
        throw AutenticarException("Email já cadastrado");
      }
    }
  }

  logar(String email, String senha) async {
    try { // try catch para a senha
      await _auth.signInWithEmailAndPassword(email: email, password: senha);
      _getUsuario();
    }
    on FirebaseAuthException catch (e) {
      if (e.code == 'user-not-found') {
        throw AutenticarException("Email não cadastrado");
      }
      else if (e.code == 'wrong-passowrd') {
        throw AutenticarException("Senha incorreta");
      }
    }
  }

  recuperar(String email) async {
    try { // try catch para a senha
      await _auth.sendPasswordResetEmail(email: email);
      _getUsuario();
    }
    on FirebaseAuthException catch (e) {
      if (e.code == 'user-not-found') {
        throw AutenticarException("Email não encontrado");
      }
    }
  }

  sair() async {
    await _auth.signOut();
    _getUsuario();
  }

}