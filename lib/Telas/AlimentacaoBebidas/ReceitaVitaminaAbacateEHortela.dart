import 'package:flutter/material.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';

void main() => runApp(ReceitaVitaminaAbacateEHortela());

class ReceitaVitaminaAbacateEHortela extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return layoutReceitas(
        context,
        'Abacate e hortelã',
        '↪ 1 abacate;\n↪ 5 folhas frescas de hortelã;\n↪ 1 copo de leite;\n↪ Açúcar a gosto.',
        '↪ Bata todos os ingredientes no liquidificador e sirva gelado.',
        'Bebidas');
  }
}
