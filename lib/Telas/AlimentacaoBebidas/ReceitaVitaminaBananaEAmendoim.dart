import 'package:flutter/material.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';

void main() => runApp(ReceitaVitaminaBananaEAmendoim());

class ReceitaVitaminaBananaEAmendoim extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return layoutReceitas(
        context,
        'Banana e amendoim',
        '↪ 200 ml de bebida vegetal de sua preferência (arroz, amêndoa, aveia, soja) ou leite desnatado;\n↪ 1 banana prata congelada;\n↪ 1 colher (chá) de cacau em pó;\n↪ 1 colher (chá) de pasta de amendoim integral;\n↪ 1 colher (sobremesa) de semente de chia.',
        '↪ Bata todos os ingredientes no liquidificador e sirva gelado.',
        'Bebidas');
  }
}
