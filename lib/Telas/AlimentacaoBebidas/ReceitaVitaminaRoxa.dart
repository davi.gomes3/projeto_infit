import 'package:flutter/material.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';

void main() => runApp(ReceitaVitaminaRoxa());

class ReceitaVitaminaRoxa extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return layoutReceitas(
        context,
        'Vitamina roxa',
        '↪ 3 colheres de sopa de leite;\n↪ 2 bolas de creme de açaí;\n↪ 2 bolas de sorvete de chocolate light;\n↪ 4 folhas de hortelã;\n↪ 1 xícaras de chá de morangos picados',
        '↪ Bata no liquidificador o leite, o açaí, o sorvete de chocolate e a hortelã até ficar homogêneo; \n\n↪ Junte depois os morangos e bata na função pulsar do liquidificador até que os morangos estejam misturados à massa; \n\n↪ Decore as laterais de um copo com a calda de chocolate e preencha com a massa obtida. Decore com folhas de hortelã e morangos picados. Sirva em seguida.',
        'Bebidas');
  }
}
