import 'package:flutter/material.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';

void main() => runApp(ReceitaVitaminaLaranja());

class ReceitaVitaminaLaranja extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return layoutReceitas(
        context,
        'Vitamina laranja',
        '↪ 1 mamão;\n↪ 1 cenoura ralada;\n↪ 2 copos de suco de laranja.',
        '↪ Bata todos os ingredientes no liquidificador e sirva gelado.',
        'Bebidas');
  }
}
