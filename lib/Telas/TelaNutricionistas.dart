import 'package:flutter/material.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';
import 'package:google_fonts/google_fonts.dart';

class TelaNutricionistas extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(corFundo),
        body: SingleChildScrollView(
          child: SafeArea(
            child: Container(
              padding: EdgeInsets.only(
                top: 20,
                right: 40,
                left: 40,
              ),
              child: Column(
                children: [
                  Row(
                    children: [
                      CircleAvatar(
                        radius: 20,
                        backgroundColor: Color(corSuporteFundoVerde),
                        child: IconButton(
                          icon: const Icon(Icons.arrow_back),
                          color: Color(corSuporteFundo),
                          onPressed: () => Navigator.pushNamed(context, 'Menu'),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(
                          top: 20,
                          right: 10,
                          left: 20,
                        ),
                      ),
                      Text(
                        'Nutricionistas \nno instagram',
                        textAlign: TextAlign.center,
                        style: GoogleFonts.getFont(fonte,
                            color: Color(corSuporteTexto),
                            fontSize: tamanhoLetraTitulos,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                  Card(
                    margin: EdgeInsets.only(
                      top: 20,
                    ),
                    color: Colors.yellow[200],
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0),
                    ),
                    shadowColor: Colors.black,
                    elevation: 7,
                    child: ListTile(
                      title: Text('@aleluglio',
                          style: GoogleFonts.dosis().copyWith(
                              color: Colors.black,
                              fontSize: tamanhoLetraSubtitulos,
                              fontWeight: FontWeight.bold)),
                    ),
                  ),
                  Card(
                    margin: EdgeInsets.only(
                        right: 22, left: 22, top: 10, bottom: 20),
                    elevation: tamanhoSombra,
                    shadowColor: Colors.black,
                    color: Color(corSuporteFundo),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Card(
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(50),
                            ),
                            child: new Image.asset(
                              'assets/imagens/nutricionistas/aleluglio.jpeg',
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Card(
                    margin: EdgeInsets.only(
                      top: 20,
                    ),
                    color: Colors.yellow[200],
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0),
                    ),
                    shadowColor: Colors.black,
                    elevation: 7,
                    child: ListTile(
                      title: Text('@fernandascheernutri',
                          style: GoogleFonts.dosis().copyWith(
                              color: Colors.black,
                              fontSize: tamanhoLetraSubtitulos,
                              fontWeight: FontWeight.bold)),
                    ),
                  ),
                  Card(
                    margin: EdgeInsets.only(
                        right: 22, left: 22, top: 10, bottom: 20),
                    elevation: tamanhoSombra,
                    shadowColor: Colors.black,
                    color: Color(corSuporteFundo),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Card(
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(50),
                            ),
                            child: new Image.asset(
                              'assets/imagens/nutricionistas/fernanda.jpeg',
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Card(
                    margin: EdgeInsets.only(
                      top: 20,
                    ),
                    color: Colors.yellow[200],
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0),
                    ),
                    shadowColor: Colors.black,
                    elevation: 7,
                    child: ListTile(
                      title: Text('@patriciadavidson.nutri',
                          style: GoogleFonts.dosis().copyWith(
                              color: Colors.black,
                              fontSize: tamanhoLetraSubtitulos,
                              fontWeight: FontWeight.bold)),
                    ),
                  ),
                  Card(
                    margin: EdgeInsets.only(
                        right: 22, left: 22, top: 10, bottom: 20),
                    elevation: tamanhoSombra,
                    shadowColor: Colors.black,
                    color: Color(corSuporteFundo),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Card(
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(50),
                            ),
                            child: new Image.asset(
                              'assets/imagens/nutricionistas/patricia.jpeg',
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Card(
                    margin: EdgeInsets.only(
                      top: 20,
                    ),
                    color: Colors.yellow[200],
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0),
                    ),
                    shadowColor: Colors.black,
                    elevation: 7,
                    child: ListTile(
                      title: Text('@sophiederam.br',
                          style: GoogleFonts.dosis().copyWith(
                              color: Colors.black,
                              fontSize: tamanhoLetraSubtitulos,
                              fontWeight: FontWeight.bold)),
                    ),
                  ),
                  Card(
                    margin: EdgeInsets.only(
                        right: 22, left: 22, top: 10, bottom: 20),
                    elevation: tamanhoSombra,
                    shadowColor: Colors.black,
                    color: Color(corSuporteFundo),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Card(
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(50),
                            ),
                            child: new Image.asset(
                              'assets/imagens/nutricionistas/sophie.jpeg',
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Card(
                    margin: EdgeInsets.only(
                      top: 20,
                    ),
                    color: Colors.yellow[200],
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0),
                    ),
                    shadowColor: Colors.black,
                    elevation: 7,
                    child: ListTile(
                      title: Text('@nutritahim',
                          style: GoogleFonts.dosis().copyWith(
                              color: Colors.black,
                              fontSize: tamanhoLetraSubtitulos,
                              fontWeight: FontWeight.bold)),
                    ),
                  ),
                  Card(
                    margin: EdgeInsets.only(
                        right: 22, left: 22, top: 10, bottom: 20),
                    elevation: tamanhoSombra,
                    shadowColor: Colors.black,
                    color: Color(corSuporteFundo),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Card(
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(50),
                            ),
                            child: new Image.asset(
                              'assets/imagens/nutricionistas/nutritahim.jpeg',
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Card(
                    margin: EdgeInsets.only(
                      top: 20,
                    ),
                    color: Colors.yellow[200],
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0),
                    ),
                    shadowColor: Colors.black,
                    elevation: 7,
                    child: ListTile(
                      title: Text('@nutrifergiarola',
                          style: GoogleFonts.dosis().copyWith(
                              color: Colors.black,
                              fontSize: tamanhoLetraSubtitulos,
                              fontWeight: FontWeight.bold)),
                    ),
                  ),
                  Card(
                    margin: EdgeInsets.only(
                        right: 22, left: 22, top: 10, bottom: 20),
                    elevation: tamanhoSombra,
                    shadowColor: Colors.black,
                    color: Color(corSuporteFundo),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Card(
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(50),
                            ),
                            child: new Image.asset(
                              'assets/imagens/nutricionistas/nutrifer.jpeg',
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Card(
                    margin: EdgeInsets.only(
                      top: 20,
                    ),
                    color: Colors.yellow[200],
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0),
                    ),
                    shadowColor: Colors.black,
                    elevation: 7,
                    child: ListTile(
                      title: Text('@_papodenutri',
                          style: GoogleFonts.dosis().copyWith(
                              color: Colors.black,
                              fontSize: tamanhoLetraSubtitulos,
                              fontWeight: FontWeight.bold)),
                    ),
                  ),
                  Card(
                    margin: EdgeInsets.only(
                        right: 22, left: 22, top: 10, bottom: 20),
                    elevation: tamanhoSombra,
                    shadowColor: Colors.black,
                    color: Color(corSuporteFundo),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Card(
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(50),
                            ),
                            child: new Image.asset(
                              'assets/imagens/nutricionistas/papodenutri.jpeg',
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  botaoSemBorda('Retornar', 'MenuAlimentos', corSuporteTexto, context),
                  pularLinha(20),
                ],
              ),
            ),
          ),
        ),
      );
  }
}
