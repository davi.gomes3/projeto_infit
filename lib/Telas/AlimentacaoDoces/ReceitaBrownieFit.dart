import 'package:flutter/material.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';

void main() => runApp(ReceitaBrownieFit());

class ReceitaBrownieFit extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return layoutReceitas(
        context,
        'Brownie Fit',
        '↪ 4 unidades de batata doce;\n↪ 1/2 xícara de farinha de amêndoas;\n↪ 1/2 xícara de óleo de coco;\n↪ 1/2 xícara de cacau em pó 100%;\n↪ 1 ovo;\n↪ 1/2 xícara de mel;\n↪ 1 colher de sopa de extrato de baunilha;\n↪ 1 pitada de sal;\n↪ 1/2 xícara de noz pecan;\n↪ 1 colher de chá de bicarbonato de sódio.',
        '↪ Embrulhe as batatas doces em papel alumínio e asse-as em forno preaquecido a 160˚C por aproximadamente 1h ou até que estejam tenras;\n\n↪ Retire a polpa das batatas e preencha 1 e 1/2 xícara de chá;\n\n↪ No processador, junte essa medida de batata doce com a farinha de amêndoas, o óleo de coco, o cacau em pó, o ovo, o mel, o extrato de baunilha e a pitada de sal. Processe até obter uma mistura homogênea;\n\n↪ Acrescente o bicarbonato de sódio, processe novamente e então adicione as nozes picadas;\n\n↪ Processe até que tudo se misture e, em seguida, despeje a massa em uma forma de 30 x 10 cm forrada com papel manteiga;\n\n↪ Asse em forno preaquecido a 170˚C por aproximadamente 40 minutos.',
        'TelaDoces');
  }
}
