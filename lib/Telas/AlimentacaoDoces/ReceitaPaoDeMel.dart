import 'package:flutter/material.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';

void main() => runApp(ReceitaPaoDeMel());

class ReceitaPaoDeMel extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return layoutReceitas(
        context,
        'Pão de mel',
        '↪ 2 1/2 xícaras (chá) farinha de trigo integral;\n↪ 1 xícara (chá) mel;\n↪ 3 colheres (chá) cacau em pó;\n↪ 2 xícaras (chá) água ou água de coco;\n↪ 1 colher (chá) canela;\n↪ 1/2 colher (café) gengibre em pó (opcional);\n↪ 1/2 colher (café) noz moscada (opcional);\n↪ 1/2 colher (café) cravo da índia em pó (opcional);\n↪ 1 colher (café) essência de baunilha (opcional);\n↪ 1 colher (chá) bicarbonato;\n↪ 1 colher (chá) fermento;\n↪ 130g chocolate meio amargo light.',
        '↪ Bata todos os ingredientes com a batedeira ou a mão, com exceção do chocolate;\n\n↪ Unte uma travessa e polvilhe farinha de trigo;\n\n↪ Coloque a massa e leve ao forno (190° C) por aproximadamente 25 minutos;\n\n↪ Pique o chocolate e coloque no microondas por 2 minutos, mexendo de vez em quando;\n\n↪ Desenforme o bolo e cubra com o chocolate derretido;\n\n↪ Leve à geladeira para endurecer o chocolate por cerca de 20 minutos;\n\n↪ Corte em quadradinhos.',
        'TelaDoces');
  }
}
