import 'package:flutter/material.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';

void main() => runApp(ReceitaDocinhoDeCoco());

class ReceitaDocinhoDeCoco extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return layoutReceitas(
        context,
        'Docinho de coco',
        '↪ 200 g creme de leite;\n↪ 100 g coco ralado seco; \n↪ 200 ml leite de coco;\n↪ A gosto: adoçante, coco ralado e cravo da índia.',
        '↪ Colocar o creme de leite, coco ralado e leite de coco na panela e mexa até dar uma encorpada;\n\n↪ Misture o adoçante à gosto e leve à geladeira;\n\n↪ Enrole as bolinhas e passe no coco ralado, colocando os cravinhos em cima. Ou coma de colher;\n\n↪ Mantenha guardado na geladeira.',
        'TelaDoces');
  }
}
