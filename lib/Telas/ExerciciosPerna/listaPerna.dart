import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';
import 'package:circular_countdown_timer/circular_countdown_timer.dart';

void main() => runApp(ListaPerna());

class ListaPerna extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          backgroundColor: Color(corFundo),
          body: SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.only(
                top: 20,
                right: 40,
                left: 40,
              ),
              child: SafeArea(
                child: Column(children: [
                  Column(
                    children: [
                      Container(
                        padding: EdgeInsets.only(
                          top: 20,
                          right: 20,
                          left: 40,
                        ),
                      ),
                      Center(
                        child: Text(
                          'Lista de exercícios Perna',
                          style: GoogleFonts.getFont(fonte,
                              color: Color(corSuporteTexto),
                              fontSize: 20,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(
                          top: 20,
                          right: 20,
                          left: 40,
                        ),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(top: 100),
                            child: Center(
                              child: Column(children: [
                                ElevatedButton(
                                    onPressed: () =>
                                        Navigator.pushNamed(context, 'perna1'),
                                    child: SizedBox(
                                        width: 200,
                                        height: 30,
                                        child: Center(
                                            child: Text('Exercicio 1')))),
                                ElevatedButton(
                                    onPressed: () =>
                                        Navigator.pushNamed(context, 'perna2'),
                                    child: SizedBox(
                                        width: 200,
                                        height: 30,
                                        child: Center(
                                            child: Text('Exercicio 2')))),
                                ElevatedButton(
                                    onPressed: () =>
                                        Navigator.pushNamed(context, 'perna3'),
                                    child: SizedBox(
                                        width: 200,
                                        height: 30,
                                        child: Center(
                                            child: Text('Exercicio 3')))),
                                ElevatedButton(
                                    onPressed: () =>
                                        Navigator.pushNamed(context, 'perna4'),
                                    child: SizedBox(
                                        width: 200,
                                        height: 30,
                                        child: Center(
                                            child: Text('Exercicio 4')))),
                                ElevatedButton(
                                    onPressed: () =>
                                        Navigator.pushNamed(context, 'perna5'),
                                    child: SizedBox(
                                        width: 200,
                                        height: 30,
                                        child: Center(
                                            child: Text('Exercicio 5')))),
                                ElevatedButton(
                                    onPressed: () =>
                                        Navigator.pushNamed(context, 'perna6'),
                                    child: SizedBox(
                                        width: 200,
                                        height: 30,
                                        child: Center(
                                            child: Text('Exercicio 6')))),
                              ]),
                            ),
                          )

                          // Container(
                          //   padding: EdgeInsets.only(
                          //     top: 20,
                          //     right: 20,
                          //     left: 40,
                          //   ),
                          // ),
                          // botaoCorEstado('comFrutasBanana', 'Banana', context,
                          //     'assets/imagens/comFrutas/banana.jpg'),
                          // Container(
                          //       padding: EdgeInsets.only(
                          //         top: 20,
                          //         right: 20,
                          //         left: 40,
                          //       ),
                          //     ),
                          //     botaoCorEstado('TelaComFrutasAbacate', 'Abacate',
                          //         context, 'assets/imagens/comFrutas/abacate.jpg'),
                          //     Container(
                          //       padding: EdgeInsets.only(
                          //         top: 20,
                          //         right: 20,
                          //         left: 40,
                          //       ),
                          //     ),
                          //     botaoCorEstado('TelaComFrutasMelancia', 'Melancia',
                          //         context, 'assets/imagens/comFrutas/melancia.jpg'),
                          //   ],
                          // ),
                          // Column(
                          //   children: [
                          //     Container(
                          //       padding: EdgeInsets.only(
                          //         top: 20,
                          //         right: 20,
                          //         left: 40,
                          //       ),
                          //     ),
                          //     botaoCorEstado('TelaComFrutasAbacaxi', 'Abacaxi',
                          //         context, 'assets/imagens/comFrutas/abacaxi.jpg'),
                          //     Container(
                          //       padding: EdgeInsets.only(
                          //         top: 20,
                          //         right: 20,
                          //         left: 40,
                          //       ),
                          //     ),
                          //     botaoCorEstado('TelaComFrutasKiwi', 'Kiwi', context,
                          //         'assets/imagens/comFrutas/kiwi.jpg'),
                          //     Container(
                          //       padding: EdgeInsets.only(
                          //         top: 20,
                          //         right: 20,
                          //         left: 40,
                          //       ),
                          //     ),
                          //     botaoCorEstado('TelaComFrutasMorango', 'Morango',
                          //         context, 'assets/imagens/comFrutas/morango.jpg'),
                        ],
                      ),
                    ],
                  ),
                  Container(
                    padding: EdgeInsets.only(
                      top: 20,
                      right: 20,
                      left: 40,
                    ),
                  ),
                  TextButton(
                      onPressed: () => Navigator.of(context).pop(),
                      child: Text("Retornar"))
                ]),
              ),
            ),
          ),
        ));
  }
}
