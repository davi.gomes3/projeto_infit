import 'package:flutter/material.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';

void main() => runApp(ReceitaGeleiaDeMelancia());

class ReceitaGeleiaDeMelancia extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return layoutReceitas(
        context,
        'Geleia de melancia',
        '↪ 1/4 de melancia (para render 1 litro de suco);\n↪ 1 fava de baunilha;\n↪ 1 limão;\n↪ 1 maçã; \n↪ 1/2 xícara de açúcar demerara ou cristal',
        '↪ Bater a melancia no liquidificador e passar em uma peneira para tirar as sementes. Deve render 1 litro de suco;\n\n↪ Adicionar em uma panela. Raspar a fava da baunilha e colocar as sementinhas na panela. Adicionar também o suco de um limão, uma maçã cortada em fatias finas e 1/2 xícara de açúcar;\n\n↪  Ligar em fogo médio baixo e cuidar até que fique em ponto de geleia. Leva entre 20 e 30 minutos;\n\n↪ Retirar as maçãs, que podem ser consumidas como sobremesa, e colocar a geleia em um vidro esterilizado.',
        'TelaComFrutasMelancia');
  }
}
