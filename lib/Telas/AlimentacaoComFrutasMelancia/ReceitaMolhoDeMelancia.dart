import 'package:flutter/material.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';

void main() => runApp(ReceitaMolhoDeMelancia());

class ReceitaMolhoDeMelancia extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return layoutReceitas(
        context,
        'Molho de melancia',
        '↪ Casca de meio kg de melancia cortada em pedaços;\n↪ 3 dentes de alho;\n↪ 1 tomate picado;\n↪ 2 copos de molho de tomate;\n↪ Suco de 1/2 limão; \n↪ Azeite e temperos a gosto.',
        '↪ Lave bem e corte em pedaços pequenos a casca, em uma panela adicione o azeite e o alho, doure um pouco e acrescente os temperos em pó;\n\n↪ Adicione na panela o tomate picado e um pouquinho de sal para só então colocar a casca da melancia;\n\n↪  Em fogo baixo adicione o suco de limão, misture bem e adicione o molho de tomate e deixe a panela entreaberta em fogo baixo, vá mexendo as vezes e adicione água sempre que o molho estiver secando até que a melancia fique macia; \n\n↪ Finalize colocando mais molho de tomate e ajustando os temperos.',
        'TelaComFrutasMelancia');
  }
}
