import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';

void main() => runApp(TelaComFrutasMelancia());

class TelaComFrutasMelancia extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          backgroundColor: Color(corFundo),
          body: SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.only(
                top: 20,
                right: 40,
                left: 40,
              ),
              child: SafeArea(
                child: Column(children: [
                  Column(
                    children: [
                      Container(
                        padding: EdgeInsets.only(
                          top: 20,
                          right: 20,
                          left: 40,
                        ),
                      ),
                      Center(
                        child: Text(
                          'Com frutas: melancia',
                          style: GoogleFonts.getFont(fonte,
                              color: Color(corSuporteTexto),
                              fontSize: 20,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(
                          top: 20,
                          right: 20,
                          left: 40,
                        ),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Column(
                        children: [
                          Container(
                            padding: EdgeInsets.only(
                              top: 20,
                              right: 20,
                              left: 40,
                            ),
                          ),
                          botaoCorEstado(
                              'ReceitaGeleiaDeMelancia',
                              'Geleia de melancia',
                              context,
                              'assets/imagens/comFrutas/comFrutasMelancia/geleia-de-melancia.jpg'),
                          Container(
                            padding: EdgeInsets.only(
                              top: 20,
                              right: 20,
                              left: 40,
                            ),
                          ),
                          botaoCorEstado(
                              'ReceitaGranitaRosa',
                              'Granita rosa',
                              context,
                              'assets/imagens/comFrutas/comFrutasMelancia/granita-de-melancia.jpg'),
                          Container(
                            padding: EdgeInsets.only(
                              top: 20,
                              right: 20,
                              left: 40,
                            ),
                          ),
                          botaoCorEstado(
                              'ReceitaMolhoDeMelancia',
                              'Molho de melancia',
                              context,
                              'assets/imagens/comFrutas/comFrutasMelancia/molho-de-melancia.jpg'),
                        ],
                      ),
                      Column(
                        children: [
                          Container(
                            padding: EdgeInsets.only(
                              top: 20,
                              right: 20,
                              left: 40,
                            ),
                          ),
                          botaoCorEstado(
                              'ReceitaSaladaComMelancia',
                              'Salada',
                              context,
                              'assets/imagens/comFrutas/comFrutasMelancia/salada-com-melancia.jpg'),
                          Container(
                            padding: EdgeInsets.only(
                              top: 20,
                              right: 20,
                              left: 40,
                            ),
                          ),
                          botaoCorEstado(
                              'ReceitaPicoleDeMelancia',
                              'Picolé de melancia',
                              context,
                              'assets/imagens/comFrutas/comFrutasMelancia/picole-de-melancia.jpg'),
                          Container(
                            padding: EdgeInsets.only(
                              top: 20,
                              right: 20,
                              left: 40,
                            ),
                          ),
                          botaoCorEstado(
                              'ReceitaSucoDeMelancia',
                              'Suco rosa',
                              context,
                              'assets/imagens/comFrutas/comFrutasMelancia/suco-de-melancia.jpg'),
                        ],
                      ),
                    ],
                  ),
                  Container(
                    padding: EdgeInsets.only(
                      top: 20,
                      right: 20,
                      left: 40,
                    ),
                  ),
                  botaoSemBorda(
                      'Retornar', 'comFrutas', corSuporteTexto, context)
                ]),
              ),
            ),
          ),
        ));
  }
}
