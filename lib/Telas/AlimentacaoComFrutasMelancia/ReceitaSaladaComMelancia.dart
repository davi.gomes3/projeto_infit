import 'package:flutter/material.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';

void main() => runApp(ReceitaSaladaComMelancia());

class ReceitaSaladaComMelancia extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return layoutReceitas(
        context,
        'Salada com melancia',
        '↪ Melancia e laranja cortadas em pedaços;\n↪ Ricota vegana caseira ou light.',
        '↪ Amasse a ricota com um garfo;\n\n↪ Misture todos os ingredientes;\n\n↪  Finalize com folhinhas de salsinha, azeite, sal e uma pitada de pimenta do reino moída na hora.',
        'TelaComFrutasMelancia');
  }
}
