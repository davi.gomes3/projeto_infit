import 'package:flutter/material.dart';
import 'package:testeteladelogin/Model/metodosTela.dart';

class Teste extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(corFundo),
      body: Container(
        padding: EdgeInsets.only(
          top: 20,
          right: 40,
          left: 40,
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            pularLinha(20),
            caixaDeTexto("Exercícios", corPrimaria, 30, TextAlign.center),
            pularLinha(20),
            caixaDeTexto('O que você deseja exercitar hoje?', corPrimaria, 15,
                TextAlign.center),
            pularLinha(40),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                botaoCorEstado('telaBraco', 'Braço', context,
                    'assets/imagens/teste/braço.png'),
                botaoCorEstado('telaCosta', 'Costas', context,
                    'assets/imagens/teste/costa.png'),
              ],
            ),
            pularLinha(20),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                botaoCorEstado('telaPeito', 'Peito', context,
                    'assets/imagens/teste/peito.png'),
                botaoCorEstado('telaAbd', 'Abdômen', context,
                    'assets/imagens/teste/abdomen.png'),
              ],
            ),
            pularLinha(20),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                botaoCorEstado('telaPerna', 'Perna', context,
                    'assets/imagens/teste/perna.png'),
                botaoCorEstado('telaAlong', 'Alongamento', context,
                    'assets/imagens/teste/alongamento.png'),
              ],
            ),
            pularLinha(40),
            botaoSemBorda("Retornar", 'menu', corSecundaria, context),
          ],
        ),
      ),
    );
  }
}
