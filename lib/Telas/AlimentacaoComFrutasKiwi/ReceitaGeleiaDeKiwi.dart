import 'package:flutter/material.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';

void main() => runApp(ReceitaGeleiaDeKiwi());

class ReceitaGeleiaDeKiwi extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return layoutReceitas(
        context,
        'Geleia de kiwi',
        '↪ 5 kiwis;\n↪ 1/2 xícara de açúcar;\n↪ 1 maçã sem casca e sem sementes ralada.',
        '↪ Triture ou corte o kiwi e a maçã em pedaços bem pequenos;\n\n↪ Em uma palena, junte as frutas e o açúcar, e mexa em fogo baixo até que fiquem molinhas e tenham reduzido até a consistência de geleia;\n\n↪ Disponha a mistura em um potinho com tampa.',
        'TelaComFrutasKiwi');
  }
}
