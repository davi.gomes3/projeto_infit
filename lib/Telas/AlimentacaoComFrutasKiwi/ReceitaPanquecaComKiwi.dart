import 'package:flutter/material.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';

void main() => runApp(ReceitaPanquecaComKiwi());

class ReceitaPanquecaComKiwi extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return layoutReceitas(
        context,
        'Panqueca com kiwi',
        '↪ 2 ovos;\n↪ 3 colheres de sopa de farinha de trigo;\n↪ 200 ml de leite integral;\n↪ 1 kiwi maduro;\n↪ 1/2 manga média madura;\n↪ 150ml de água; \n↪ 3 colheres de açúcar; \n↪ Mel.',
        '↪ Em um liquidificador, bata os ovos, a farinha e o leite até obter uma massa homogênea;\n\n↪ Pique as frutas e coloque numa panela com a água e o açúcar. Mexa de vez em quando. A calda estará pronta quando a calda engrossar e as frutas ficarem bem molinhas;\n\n↪ Disponha as panquecas uma sobre a outra.Por cima coloque a calda e regue com mel à gosto.',
        'TelaComFrutasKiwi');
  }
}
