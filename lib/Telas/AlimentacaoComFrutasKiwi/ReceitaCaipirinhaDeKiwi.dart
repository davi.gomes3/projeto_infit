import 'package:flutter/material.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';

void main() => runApp(ReceitaCaipirinhaDeKiwi());

class ReceitaCaipirinhaDeKiwi extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return layoutReceitas(
        context,
        'Caipirinha de kiwi',
        '↪ 2 kiwis descascados;\n↪ 60 ml de saquê;\n↪ Açúcar a gosto; \n↪ Açúcar e cubos de gelo a gosto.',
        '↪ Coloque o kiwi e o açúcar em um copo e amasse com o pilão. Acrescente o saquê;\n\n↪ Acrescente cubos de gelo e mexa.',
        'TelaComFrutasKiwi');
  }
}
