import 'package:flutter/material.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';

void main() => runApp(ReceitaBeijinhoDeMorango());

class ReceitaBeijinhoDeMorango extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return layoutReceitas(
        context,
        'Beijinho de morango',
        '↪ 7 colheres (sopa) de coco;\n↪ 5 colheres (sopa) de leite em pó desnatado;\n↪ 2 colheres (sopa) de adoçante;\n↪ Morangos cortados.',
        '↪ Misture todos os ingredientes (exceto o morango) até virar uma massa. Depois leve a massa para a geladeira por 30 minutos;\n\n↪ Retire da geladeira, pegue os morangos já cortados, coloque um pouco de massa na mão, coloque o morango e enrole;\n\n↪ Passe no coco ralado. Agora é só servir. ',
        'TelaComFrutasMorango');
  }
}
