import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';

void main() => runApp(TelaComFrutasMorango());

class TelaComFrutasMorango extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          backgroundColor: Color(corFundo),
          body: SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.only(
                top: 20,
                right: 40,
                left: 40,
              ),
              child: SafeArea(
                child: Column(children: [
                  Column(
                    children: [
                      Container(
                        padding: EdgeInsets.only(
                          top: 20,
                          right: 20,
                          left: 40,
                        ),
                      ),
                      Center(
                        child: Text(
                          'Com frutas: morango',
                          style: GoogleFonts.getFont(fonte,
                              color: Color(corSuporteTexto),
                              fontSize: 20,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(
                          top: 20,
                          right: 20,
                          left: 40,
                        ),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Column(
                        children: [
                          Container(
                            padding: EdgeInsets.only(
                              top: 20,
                              right: 20,
                              left: 40,
                            ),
                          ),
                          botaoCorEstado(
                              'ReceitaCremeDeMorango',
                              'Creme de morango',
                              context,
                              'assets/imagens/comFrutas/comFrutasMorango/creme-de-aveia-e-morango.jpg'),
                          Container(
                            padding: EdgeInsets.only(
                              top: 20,
                              right: 20,
                              left: 40,
                            ),
                          ),
                          botaoCorEstado(
                              'ReceitaSmoothieDeMorango',
                              'Smoothie',
                              context,
                              'assets/imagens/comFrutas/comFrutasMorango/smoothie-de-morango.jpeg'),
                          Container(
                            padding: EdgeInsets.only(
                              top: 20,
                              right: 20,
                              left: 40,
                            ),
                          ),
                          botaoCorEstado(
                              'ReceitaPanquecaComMorango',
                              'Panqueca com \nmorango',
                              context,
                              'assets/imagens/comFrutas/comFrutasMorango/panqueca-com-morango.jpg'),
                        ],
                      ),
                      Column(
                        children: [
                          Container(
                            padding: EdgeInsets.only(
                              top: 20,
                              right: 20,
                              left: 40,
                            ),
                          ),
                          botaoCorEstado(
                              'ReceitaSaladaComMorango',
                              'Salada',
                              context,
                              'assets/imagens/comFrutas/comFrutasMorango/salada-com-morango.jpg'),
                          Container(
                            padding: EdgeInsets.only(
                              top: 20,
                              right: 20,
                              left: 40,
                            ),
                          ),
                          botaoCorEstado(
                              'ReceitaGeleiaDeMorango',
                              'Geleia de morango',
                              context,
                              'assets/imagens/comFrutas/comFrutasMorango/geleia-de-morango.jpg'),
                          Container(
                            padding: EdgeInsets.only(
                              top: 20,
                              right: 20,
                              left: 40,
                            ),
                          ),
                          botaoCorEstado(
                              'ReceitaBeijinhoDeMorango',
                              'Docinho de \nmorango ',
                              context,
                              'assets/imagens/comFrutas/comFrutasMorango/beijinho-de-morango.jpg'),
                        ],
                      ),
                    ],
                  ),
                  Container(
                    padding: EdgeInsets.only(
                      top: 20,
                      right: 20,
                      left: 40,
                    ),
                  ),
                  botaoSemBorda(
                      'Retornar', 'comFrutas', corSuporteTexto, context)
                ]),
              ),
            ),
          ),
        ));
  }
}
