import 'package:flutter/material.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';

void main() => runApp(ReceitaGeleiaDeMorango());

class ReceitaGeleiaDeMorango extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return layoutReceitas(
        context,
        'Geleia de morango',
        '↪ Suco de meio limão;\n↪ 1 xícara de chá de água;\n↪ Adoçante a gosto;\n↪ 1 caixa de morangos maduros picados.',
        '↪ Dentro de uma panela, coloque os morango e a água;\n\n↪ Quando ferver, misture até engrossar;\n\n↪ Quando reduzir e estiver em consistência de geléia, transfira para uma vasilha com tampa. ',
        'TelaComFrutasMorango');
  }
}
