import 'package:flutter/material.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';

void main() => runApp(ReceitaHamburguerVegano());

class ReceitaHamburguerVegano extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return layoutReceitas(
        context,
        'Hambúrguer vegano',
        '↪ 1 xícara de cebola branca picada;\n↪ Fio de azeite para untar a frigideira;\n↪ 2 dentes de alho picado ou amassado;\n↪ 1/2 xícara de beterraba ralada;\n↪ 1/2 xícara de cenoura ralada;\n↪ 1 colher de sopa de molho shoyo;\n↪ Pimenta caiena a gosto (opcional);\n↪ Suco de 1/2 limão;\n↪ 2 xícaras de feijão cozido;\n↪ 3/2 de xícara de fubá;\n↪ Sal a gosto.',
        '↪ Refogar a cebola e alho em um fio de azeite até murchar. Adicionar a beterraba, cenoura, shoyo, suco de meio limão e uma pitada de pimenta caiena. Refogar por 10 minutos;\n\n↪ Em um processador de alimentos ou liquidificador, adicionar o feijão, o refogado da panela e uma pitada de sal, adicionando o fubá aos poucos;\n\n↪ Retirar o formar os hambúrgueres do tamanho desejado envolvendo cada hambúrguer com um pouco de fubá;\n\n↪ Coloque os hambúrgueres em uma forma untada com azeite e levar ao forno médio para assar por cerca de 10 minutos de cada lado;\n\n↪ Monte os hambúrgueres com a salada e molhos de preferência.',
        'TelaVeganas');
  }
}
