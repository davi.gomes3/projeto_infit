import 'package:flutter/material.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';

void main() => runApp(ReceitaCheddarVegano());

class ReceitaCheddarVegano extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return layoutReceitas(
        context,
        'Cheddar vegano',
        '↪ 1 xícara de castanha de caju crua;\n↪ 1 colher de sopa cheia de cúrcuma; \n↪ 3 colheres de sopa de cacau cru;\n↪ 3 colheres de sopa de azeite;\n↪ 1 dente de alho;\n↪ 1 colher de sopa de limão;\n↪ 1/2 xícara de água;\n↪ 1 pitada de sal.',
        '↪ Bater todos os ingredientes no liquidificador e guardar na geladeira até estar em consistência firme. \n\n↪ Caso o liquidificador não consiga bater as castanhas facilmente, deve-se deixá-las de molho em água por cerca de 20 minutos e escorrer bem antes de bater.',
        'TelaVeganas');
  }
}
