import 'package:flutter/material.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';

void main() => runApp(ReceitaToffeDeCenouraEMaca());

class ReceitaToffeDeCenouraEMaca extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return layoutReceitas(
        context,
        'Bolo toffe vegano',
        '↪ 2 maçãs descascadas e raladas;\n↪ 115 g de nozes;\n↪ 80 g de coco ralado seco;\n↪ 1/2 colher de chá de canela;\n↪ 2 colheres de sopa de algarroba;\n↪ 2 colheres de sopa de cacau cru em pó;\n↪ 1 pitada de sal marinho;\n↪ 150 g de uva-passas;\n↪ 60 g de maçã seca (demolhada por 15 minutos e escorrida);\n↪ 60 g de tâmaras sem caroço (demolhadas por 15 minutos e escorrida);\n↪ 1 laranja descascada.',
        '↪ Em uma tigela, misturar maçã e cenoura, nozes, coco, algarroba em pó, cacau cru, canela, sal e uvas-passas;\n\n↪ No liquidificador, misturar maçã seca demolhada, tâmaras e laranja, até obter massa úmida;\n\n↪ Espalhar metade do molho de tomate na assadeira;\n\n↪ Em seguida, untar forma redonda de 20 cm, com papel manteiga, pressionar a massa e a mistura na forma e levar a geladeira por 3 horas.',
        'TelaVeganas');
  }
}
