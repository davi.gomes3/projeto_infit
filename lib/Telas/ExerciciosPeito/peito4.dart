import 'package:circular_countdown_timer/circular_countdown_timer.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';

void main() => runApp(PeitoQuatro());

class PeitoQuatro extends StatefulWidget {
  @override
  _PeitoQuatroState createState() => _PeitoQuatroState();
}

class _PeitoQuatroState extends State<PeitoQuatro> {
  CountDownController _controller = CountDownController();
  int _duration = 10;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          backgroundColor: Color(corFundo),
          body: SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.only(
                top: 20,
                right: 40,
                left: 40,
              ),
              child: SafeArea(
                child: Column(children: [
                  Column(
                    children: [
                      Container(
                        padding: EdgeInsets.only(
                          top: 20,
                          right: 20,
                          left: 40,
                        ),
                      ),
                      Center(
                        child: Text(
                          'Peito Quatro',
                          style: GoogleFonts.getFont(fonte,
                              color: Color(corSuporteTexto),
                              fontSize: 20,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(
                          top: 20,
                          right: 20,
                          left: 40,
                        ),
                      ),
                    ],
                  ),
                  Column(
                    children: [
                      Image.asset('assets/imagens/exercicios/peito/peito4.gif'),
                      Padding(
                        padding: const EdgeInsets.only(top: 20.0),
                        child: CircularCountDownTimer(
                          duration: 10,
                          initialDuration: 0,
                          controller: _controller,
                          width: 140,
                          height: 140,
                          ringColor: Colors.grey,
                          ringGradient: null,
                          fillColor: Colors.purpleAccent,
                          fillGradient: null,
                          backgroundColor: Colors.purple[500],
                          backgroundGradient: null,
                          strokeWidth: 10.0,
                          strokeCap: StrokeCap.round,
                          textStyle: TextStyle(
                              fontSize: 25.0,
                              color: Colors.white,
                              fontWeight: FontWeight.bold),
                          textFormat: CountdownTextFormat.S,
                          isReverse: false,
                          isReverseAnimation: false,
                          isTimerTextShown: true,
                          autoStart: false,
                          onStart: () {
                            print('Countdown Started');
                          },
                          onComplete: () {
                            print('Countdown Ended');
                          },
                        ),
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          _button(
                              title: "Start",
                              onPressed: () => _controller.start()),
                          _button(
                              title: "Pause",
                              onPressed: () => _controller.pause()),
                        ],
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          _button(
                              title: "Resume",
                              onPressed: () => _controller.resume()),
                          _button(
                              title: "Restart",
                              onPressed: () =>
                                  _controller.restart(duration: _duration))
                        ],
                      ),
                    ],
                  ),
                  Container(
                    padding: EdgeInsets.only(
                      top: 20,
                      right: 20,
                      left: 40,
                    ),
                  ),
                  ElevatedButton(
                      onPressed: () => Navigator.of(context).pop(),
                      child: Text("Retornar"))
                ]),
              ),
            ),
          ),
        ));
  }

  _button({required String title, VoidCallback? onPressed}) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: ElevatedButton(
        child: Text(
          title,
          style: TextStyle(color: Colors.white),
        ),
        onPressed: onPressed,
      ),
    );
  }
}
