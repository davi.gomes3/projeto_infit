import 'package:flutter/material.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';

void main() => runApp(ReceitaFarofa());

class ReceitaFarofa extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return layoutReceitas(
        context,
        'Farofa com nozes',
        '↪ 1/2 unidade de cebola;\n↪ 3 dentes de alho picados;\n↪ 1 colher de manteiga ghi;\n↪ 3 unidades de ovo;\n↪ 1 xícara de chá de farinha de amêndoas;\n↪ 1/2 xícara de chá de ceblinha; \n↪ 1/4 xícara de chá de castanha de caju torrada;\n↪ 2 colheres de sopa de linhaça; \n↪1/4 xícara de chá de linhaça dourada;\n↪ 1/4 xícara de chá de sementes de girassol; \n↪1 colher de chá.',
        '↪ Refogue a cebola e o alho na manteiga até que fiquem dourados. Junte os ovos, mexendo até cozinhar; \n\n↪ Acrescente a farinha de amêndoas e misture por mais um minuto. Desligue o fogo e acrescente o restante dos ingredientes. Misture bem. Sirva em seguida.',
        'TelaLowCarb');
  }
}
