import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';

void main() => runApp(TelaLowCarb());

class TelaLowCarb extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          backgroundColor: Color(corFundo),
          body: SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.only(
                top: 20,
                right: 40,
                left: 40,
              ),
              child: SafeArea(
                child: Column(children: [
                  Column(
                    children: [
                      Container(
                        padding: EdgeInsets.only(
                          top: 20,
                          right: 20,
                          left: 40,
                        ),
                      ),
                      Center(
                        child: Text(
                          'Alimentação: low carb',
                          style: GoogleFonts.getFont(fonte,
                              color: Color(corSuporteTexto),
                              fontSize: 20,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(
                          top: 20,
                          right: 20,
                          left: 40,
                        ),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Column(
                        children: [
                          Container(
                            padding: EdgeInsets.only(
                              top: 20,
                              right: 20,
                              left: 40,
                            ),
                          ),
                          botaoCorEstado(
                              'ReceitaQuiche',
                              'Quiche de espinafre',
                              context,
                              'assets/imagens/lowCarb/low-carb-quiche.jpeg'),
                          Container(
                            padding: EdgeInsets.only(
                              top: 20,
                              right: 20,
                              left: 40,
                            ),
                          ),
                          botaoCorEstado(
                              'ReceitaRisoto',
                              'Risoto de couve',
                              context,
                              'assets/imagens/lowCarb/low-carb-risoto.jpeg'),
                          Container(
                            padding: EdgeInsets.only(
                              top: 20,
                              right: 20,
                              left: 40,
                            ),
                          ),
                          botaoCorEstado(
                              'ReceitaEspaguete',
                              'Espaguete de abobrinha',
                              context,
                              'assets/imagens/lowCarb/low-carb-espaguete.jpg'),
                        ],
                      ),
                      Column(
                        children: [
                          Container(
                            padding: EdgeInsets.only(
                              top: 20,
                              right: 20,
                              left: 40,
                            ),
                          ),
                          botaoCorEstado(
                              'ReceitaFarofa',
                              'Farofa com nozes',
                              context,
                              'assets/imagens/lowCarb/low-carb-farofa.jpeg'),
                          Container(
                            padding: EdgeInsets.only(
                              top: 20,
                              right: 20,
                              left: 40,
                            ),
                          ),
                          botaoCorEstado(
                              'ReceitaPanacota',
                              'Panacota fit',
                              context,
                              'assets/imagens/lowCarb/low-carb-panacota.jpg'),
                          Container(
                            padding: EdgeInsets.only(
                              top: 20,
                              right: 20,
                              left: 40,
                            ),
                          ),
                          botaoCorEstado(
                              'ReceitaWrapDeCouve',
                              'Wrap de couve',
                              context,
                              'assets/imagens/lowCarb/low-carb-wrap-de-couve.jpg'),
                        ],
                      ),
                    ],
                  ),
                  Container(
                    padding: EdgeInsets.only(
                      top: 20,
                      right: 20,
                      left: 40,
                    ),
                  ),
                  botaoSemBorda(
                      'Retornar', 'alimentacao', corSuporteTexto, context)
                ]),
              ),
            ),
          ),
        ));
  }
}
