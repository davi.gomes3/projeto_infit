import 'package:flutter/material.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';

void main() => runApp(ReceitaWrapDeCouve());

class ReceitaWrapDeCouve extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return layoutReceitas(
        context,
        'Wrap de couve',
        '↪ 1 folha de couve pequena orgânica;\n↪ 1/2 lata de atum ao natural; \n↪ 2 colheres (sopa) de requeijão light (ou queijo quark);\n↪ 1/2 unidade de cenoura ralada;\n↪ Cebola, alho, manjericão e alçafrão-da-terra a gosto.',
        '↪ Mergulhe a couve rapidamente na água bem quente. Coloque a folha em um prato e reserve; \n\n↪ Para o recheio, escorra bem o líquido do atum e misture o requeijão (ou queijo quark), a cenoura e os temperos;\n\n↪ Coloque a pasta sobre a couve e enrole o wrap.',
        'TelaLowCarb');
  }
}
