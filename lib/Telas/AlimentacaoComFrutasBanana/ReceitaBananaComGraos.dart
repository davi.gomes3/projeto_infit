import 'package:flutter/material.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';

void main() => runApp(ReceitaBananaComGraos());

class ReceitaBananaComGraos extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return layoutReceitas(
        context,
        ' Banana com grãos ',
        '↪ 1 banana média;\n↪ Granola, castanhas, aveia, uva passa a gosto;\n↪ Mel a gosto.',
        '↪ Amasse ou corte a banana em rodelas e reserve em um recipiente fundo;\n\n↪ Adicione a granola, a uva passa, as castanhas e o mel por cima;\n\n↪ Sirva-se!',
        'comFrutasBanana');
  }
}
