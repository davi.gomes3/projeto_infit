import 'package:flutter/material.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';

void main() => runApp(ReceitaSorveteDeBanana());

class ReceitaSorveteDeBanana extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return layoutReceitas(
        context,
        ' Sorvete de banana ',
        '↪ 300 g de bananas picadas e congeladas;\n↪ 1 colher de sopa de mel;\n↪ 1 colher de sopa de cacau em pó 100%;\n↪ Ameixa picada\n↪ Nozes picadas.',
        '↪ Em um processador, adicione as bananas, o mel e o cacau. Bata até ficar cremoso. Caso não tenha processador, bata no liquidificador, aos poucos e mexendo sempre, para não queimar;\n\n↪ Acrescente a ameixa e as nozes e misture. Transfira para um pote com tampa;\n\n↪ Deixe no congelador por, no mínimo, 5 horas. Depois, é só servir!',
        'comFrutasBanana');
  }
}
