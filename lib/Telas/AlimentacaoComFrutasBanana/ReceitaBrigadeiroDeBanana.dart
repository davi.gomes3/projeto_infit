import 'package:flutter/material.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';

void main() => runApp(ReceitaBrigadeiroDeBanana());

class ReceitaBrigadeiroDeBanana extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return layoutReceitas(
        context,
        ' Brigadeiro de banana ',
        '↪ 2 bananas maduras;\n↪ 2 colheres de sopa de cacau em pó;\n↪ 5 colheres de sopa de leite em pó (ou leite em pó de arroz).',
        '↪ Leve as bananas ao microondas por aproximadamente 3 minutos, até que estejam cozidas;\n\n↪ Descasque e amasse as bananas;\n\n↪ Misture as bananas ao cacau em pó e ao leite em pó até que obtenha ponto de brigadeiro. Caso necessário, adicione mais leite em pó.\n\n↪ Caso queira comer como brigadeiro de colher, este é o momento. \n\n↪ Se quiser enrolar, cubra com plástico filme e leve a geladeira por aproximadamente duas horas.\n\n↪ Enrole o doce em bolinhas e coloque cacau em pó por cima.',
        'comFrutasBanana');
  }
}
