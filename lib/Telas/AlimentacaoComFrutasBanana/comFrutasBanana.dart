import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';

void main() => runApp(TelaComFrutasBanana());

class TelaComFrutasBanana extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          backgroundColor: Color(corFundo),
          body: SingleChildScrollView(
            child: SafeArea(
              child: Column(children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      padding: EdgeInsets.only(
                        top: 40,
                        right: 20,
                        left: 40,
                      ),
                    ),
                    Text(
                      ' Com frutas: banana ',
                      textAlign: TextAlign.center,
                      style: GoogleFonts.getFont(fonte,
                          color: Color(corSuporteTexto),
                          fontSize: 20,
                          fontWeight: FontWeight.bold),
                    ),
                    Container(
                      padding: EdgeInsets.only(
                        top: 20,
                        right: 20,
                        left: 40,
                      ),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Column(
                      children: [
                        botaoCorEstado(
                            'receitaBananaComGraos',
                            'Banana com grãos',
                            context,
                            'assets/imagens/comFrutas/comFrutasBanana/banana_com_graos.jpg'),
                        Container(
                          padding: EdgeInsets.only(
                            top: 20,
                            right: 20,
                            left: 40,
                          ),
                        ),
                        botaoCorEstado(
                            'ReceitaBoloDeBanana',
                            'Bolo de Banana',
                            context,
                            'assets/imagens/comFrutas/comFrutasBanana/bolo_banana.jpg'),
                        Container(
                          padding: EdgeInsets.only(
                            top: 20,
                            right: 20,
                            left: 40,
                          ),
                        ),
                        botaoCorEstado(
                            'ReceitaBrigadeiroDeBanana',
                            'Brigadeiro de banana',
                            context,
                            'assets/imagens/comFrutas/comFrutasBanana/brigadeiro_banana.jpg'),
                      ],
                    ),
                    Column(
                      children: [
                        botaoCorEstado(
                            'ReceitaCookieDeBanana',
                            'Cookie de banana',
                            context,
                            'assets/imagens/comFrutas/comFrutasBanana/cookie_banana.jpg'),
                        Container(
                          padding: EdgeInsets.only(
                            top: 20,
                            right: 20,
                            left: 40,
                          ),
                        ),
                        botaoCorEstado(
                            'ReceitaPanquecaDeBanana',
                            'Panqueca de banana',
                            context,
                            'assets/imagens/comFrutas/comFrutasBanana/panqueca_banana.jpg'),
                        Container(
                          padding: EdgeInsets.only(
                            top: 20,
                            right: 20,
                            left: 40,
                          ),
                        ),
                        botaoCorEstado(
                            'ReceitaSorveteDeBanana',
                            'Sorvete de banana',
                            context,
                            'assets/imagens/comFrutas/comFrutasBanana/sorvete_banana.jpg'),
                      ],
                    ),
                  ],
                ),
                Container(
                  padding: EdgeInsets.only(
                    top: 20,
                    right: 20,
                    left: 40,
                  ),
                ),
                botaoSemBorda('Retornar', 'comFrutas', corSuporteTexto, context)
              ]),
            ),
          ),
        ));
  }
}
