import 'package:flutter/material.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';

void main() => runApp(ReceitaPaoDeQueijo());

class ReceitaPaoDeQueijo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return layoutReceitas(
        context,
        ' Pão de queijo ',
        '↪ 1 ovo\n↪ 3 colheres de tapioca hidratada;\n↪ Queijo à gosto;\n↪ Opcional: queijo cotage e farinha de amêndoas.',
        '↪ Em um liquidificador, bata todos os ingredientes;\n\n↪ Despeje a mistura em uma frigideira anti-aderente e doure o pão de queijo dos dois lados;\n\n↪ Agora, é só servir!',
        'salgadas');
  }
}
