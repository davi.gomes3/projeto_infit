import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';

void main() => runApp(TelaSalgadas());

class TelaSalgadas extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          backgroundColor: Color(corFundo),
          body: SingleChildScrollView(
            child: SafeArea(
              child: Column(children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      padding: EdgeInsets.only(
                        top: 40,
                        right: 20,
                        left: 40,
                      ),
                    ),
                    Text(
                      'Alimentação: salgadas',
                      textAlign: TextAlign.center,
                      style: GoogleFonts.getFont(fonte,
                          color: Color(corSuporteTexto),
                          fontSize: 20,
                          fontWeight: FontWeight.bold),
                    ),
                    Container(
                      padding: EdgeInsets.only(
                        top: 20,
                        right: 20,
                        left: 40,
                      ),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Column(
                      children: [
                        botaoCorEstado(
                            'ReceitaPaoDeQueijo',
                            'Pão de queijo \nde frigideira',
                            context,
                            'assets/imagens/salgadas/receita-pao-de-queijo-frigideira.jpg'),
                        Container(
                          padding: EdgeInsets.only(
                            top: 20,
                            right: 20,
                            left: 40,
                          ),
                        ),
                        botaoCorEstado(
                            'ReceitaPizzaDeFrigideira',
                            'Pizza de frigideira',
                            context,
                            'assets/imagens/salgadas/pizzaFrigideira.jpg'),
                        Container(
                          padding: EdgeInsets.only(
                            top: 20,
                            right: 20,
                            left: 40,
                          ),
                        ),
                        botaoCorEstado('ReceitaSalada', 'Salada proteica',
                            context, 'assets/imagens/salgadas/salada.jpeg'),
                      ],
                    ),
                    Column(
                      children: [
                        botaoCorEstado(
                            'ReceitaMacarraoAMilanesa',
                            'Macarrão à milanesa \nfit',
                            context,
                            'assets/imagens/salgadas/macarrao_milanesa.jpg'),
                        Container(
                          padding: EdgeInsets.only(
                            top: 20,
                            right: 20,
                            left: 40,
                          ),
                        ),
                        botaoCorEstado(
                            'ReceitaFricasse',
                            'Fricassê de frango',
                            context,
                            'assets/imagens/salgadas/fricasse_frango.jpg'),
                        Container(
                          padding: EdgeInsets.only(
                            top: 20,
                            right: 20,
                            left: 40,
                          ),
                        ),
                        botaoCorEstado('ReceitaHamburguer', 'Hambúrguer Fit',
                            context, 'assets/imagens/salgadas/hamburguer2.jpg'),
                      ],
                    ),
                  ],
                ),
                Container(
                  padding: EdgeInsets.only(
                    top: 20,
                    right: 20,
                    left: 40,
                  ),
                ),
                botaoSemBorda(
                    'Retornar', 'alimentacao', corSuporteTexto, context)
              ]),
            ),
          ),
        ));
  }
}
