import 'package:flutter/material.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';

void main() => runApp(ReceitaSalada());

class ReceitaSalada extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return layoutReceitas(
        context,
        'Salada proteica',
        '↪ 1/2 repolho;\n↪ 1 chuchu;\n↪ 2 tomates\n↪ 1/2 pimentão;\n↪ 1/2 cebola;\n↪ 1 cenoura pequena ralada; \n↪ 2 ovos cozidos;\n↪ 1 pote de iorgurte natural desnatado sem sabor;\n↪ Suco de 1 limão;\n↪ 1 colher de sopa de azeite;\n↪ Sal e pimenta do reino a gosto.',
        '↪ Corte ou rale o repolho, acrescente o xuxu ralado (cru), o pimentão, a cenoura e os tomates picados. Misture a cebola picada e os ovos cozidos;\n\n↪ Misture o iorgurte ao suco do limão em outro recipiente. Acrescente o azeite, pimenta do reino e sal;\n\n↪ Misture o molho à salada e pronto!\n\n↪ Se você preferir, pode adicionar o molho somente na hora de servir.',
        'salgadas');
  }
}
