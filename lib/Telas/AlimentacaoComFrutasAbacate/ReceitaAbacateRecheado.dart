import 'package:flutter/material.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';

void main() => runApp(ReceitaAbacateRecheado());

class ReceitaAbacateRecheado extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return layoutReceitas(
        context,
        'Abacate recheado',
        '↪ 4 abacates;\n↪ 100g de salmão defumado;\n↪ 8 ovos;\n↪ Sal, pimenta, primenta vermelha e endro fresco a gosto.',
        '↪ Acenda o forno a 200º C. Corte os abacates ao meio e retire o caroço com cuidado para não desperdiçar a polpa do abacate. Coloque as metades dos abacates por cima de um papel vegetal numa assadeira;\n\n↪ Disponha as tiras do salmão defumado. Abra cada ovo em uma taça e com a ajuda de uma colher, coloque a gema e o máximo de clara que conseguir dentro de cada metade de abacate com cuidado para não estourar. Tempere cada gema de ovo com sal e pimenta preta;\n\n↪  Leve a assadeira com cuidado ao forno e deixe assar por 20 minutos;\n\n↪ Na hora de servir coloque pimenta vermelha e o endro fresco por cima de cada abacate e sirva quente.',
        'TelaComFrutasAbacate');
  }
}
