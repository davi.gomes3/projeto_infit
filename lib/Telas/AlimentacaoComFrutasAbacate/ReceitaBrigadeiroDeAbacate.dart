import 'package:flutter/material.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';

void main() => runApp(ReceitaBrigadeiroDeAbacate());

class ReceitaBrigadeiroDeAbacate extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return layoutReceitas(
        context,
        'Brigadeiro de abacate',
        '↪ 1 xícara de abacate picado;\n↪ 2 colheres de sopa cheias de cacau em pó;\n↪ Mel a gosto.',
        '↪ Bata o abacate com o cacau no liquidificador ou mixer;\n\n↪ Coloque o mel para adoçar um pouco e bata até obter uma consistência de ganache;\n\n↪  Despeje em potinhos individuais e leve para gelar até adquirir um ponto mais firme, de ganache. Sirva.',
        'TelaComFrutasAbacate');
  }
}
