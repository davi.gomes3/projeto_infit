import 'package:flutter/material.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';

void main() => runApp(ReceitaSorveteDeAbacate());

class ReceitaSorveteDeAbacate extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return layoutReceitas(
        context,
        'Sorvete de abacate',
        '↪ 2 bananas maduras;\n↪ 1/2 abacate maduro;\n↪ 2 colheres de cacau;\n↪ 1 scoop de whey sabor chocolate;\n↪ Adoçante culinário a gosto.',
        '↪ Leve a metade do abacate e as duas bananas picadas ao congelador até congelar;\n\n↪ Bata a banana e abacate congelado no liquidificador com o restante dos ingredientes (e água, caso necessário) até obter uma consistência cremosa;\n\n↪  Despeje em potinhos individuais e sirva.',
        'TelaComFrutasAbacate');
  }
}
