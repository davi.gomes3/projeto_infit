import 'package:flutter/material.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';

void main() => runApp(ReceitaMousseDeAbacate());

class ReceitaMousseDeAbacate extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return layoutReceitas(
        context,
        'Mousse de abacate',
        '↪ 1/2 abacate maduro;\n↪ 2 bananas maduras picadas;\n↪ 3 colheres de sopa de adoçante culinário;\n↪ 1 colher de chá de essência de baunilha.',
        '↪ Coloque todos os ingredientes no liquidificador ou processador;\n\n↪ Bata até obter a consistência de um creme;\n\n↪  Despeje a mistura do creme em taças individuais e leve para gelar por uma hora. Agora é só servir!',
        'TelaComFrutasAbacate');
  }
}
