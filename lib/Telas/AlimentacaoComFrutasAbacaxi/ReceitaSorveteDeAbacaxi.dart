import 'package:flutter/material.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';

void main() => runApp(ReceitaSorveteDeAbacaxi());

class ReceitaSorveteDeAbacaxi extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return layoutReceitas(
        context,
        ' Sorvete de Abacaxi ',
        '↪ 1/2 abacaxi em pedaços congelado;\n↪ 3 bananas congeladas;\n↪ 3 colheres de mel;\n↪ Opcionais: aveia, ganola, uva passa. ',
        '↪ Bata as bananas congeladas em um processador. Adicioneo abacaxi e o mel até obter uma mistrura cremosa;\n\n↪ Caso não tenha triturador, bata no liquidificador a banana e adicione o abacaxi aos poucos, até obter uma consistência cremosa. Misture com o mel;\n\n↪ Despeje em um recipiente e, ao servir, adicione aveia, granola, uva passa e demais acompanhamentos que preferir.',
        'TelaComFrutasAbacaxi');
  }
}
