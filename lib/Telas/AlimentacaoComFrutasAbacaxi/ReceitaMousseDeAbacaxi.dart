import 'package:flutter/material.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';

void main() => runApp(ReceitaMousseDeAbacaxi());

class ReceitaMousseDeAbacaxi extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return layoutReceitas(
        context,
        ' Mousse de Abacaxi ',
        '↪ 1/2 abacaxi sem casca ;\n↪ 1 litro de leite de amêndoas;\n↪ 5 gotas de essência de baunilha;\n↪ 1 envelope de gelatina sem sabor;\n↪ 240ml de água morna.',
        '↪ Primeiro, deixe ferver por 15 minutos a água com os pedaços de abacaxi;\n\n↪ Na sequência, adicione a gelatina e leve a mistura para o liquidificador para bater com os outros ingredientes;\n\n↪ Feito isso, você pode colocar o conteúdo em taças e levar à geladeira até o mousse atingir a consistência e a temperatura desejada. Depois, é só servir!',
        'TelaComFrutasAbacaxi');
  }
}
