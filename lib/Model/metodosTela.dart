import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

// variaveis para trocar: cor de fundo, cor primaria, cor secundaria, tamanho das sombras, tamanho da letra e tamanho do raio das caixas
int corPrimaria = 0xFF32CBFF;
int corSecundaria = 0xFF00A5E0;
int corFundo = 0xFFFFFFFF;
int corSuporteFundo = 0xFFFFFFFF;
int corSombra = 0xFF333333;
double tamanhoRaio = 40;
double tamanhoSombra = 5;
double tamanhoLetra = 12;



// TAMANHOS LETRAS
//double tamanhoLetra = 15;
double tamanhoLetraTitulos = 25;
double tamanhoLetraSubtitulos = 15;

// FONTES
String fonte = 'Gothic A1';
String fonteReceitas = 'Dosis';

// método dar espaço na tela
pularLinha(double pular) {
  return SizedBox(height: pular,);
}

// método de criar um botao sem borda
botaoSemBorda(String texto, String pagina, int corTexto, BuildContext context) {
  return Container(
    child: TextButton(
      child: Text(
        texto, // texto que sera exibido
        style: GoogleFonts.getFont(
          fonte,
          fontWeight: FontWeight.bold,
          color: Color(corTexto),
          fontSize: tamanhoLetra,
        ),
      ),
    onPressed: () => Navigator.pushNamed(context, pagina), // OnPressed genérico
    ),
  );
}

// método de criar um botao com borda sem ação
botaoComBorda(String texto, String pagina, int corBotao, int corTexto, BuildContext context) {
  return Material(
    elevation: tamanhoSombra, // relevo/ tamanho da sombra
    color: Color(corBotao), // cor do botao
    shadowColor: Color(corSombra), // cor da sombra
    borderRadius: BorderRadius.all(Radius.circular(tamanhoRaio),), // raio da borda do botao
    child: botaoSemBorda(texto, pagina, corTexto, context),
  );
}

// método de criar uma caixa de texto
caixaDeTexto(String texto, int corTexto, double tamanhoFonte, TextAlign ajustar) {
  return Container(
    child: Text(
      texto, // texto que sera exibido
      textAlign: ajustar, // como ele será ajustado. ex: centro, direita, esquerda, topo direita, etc...
      style: GoogleFonts.getFont(
        fonte,
        fontWeight: FontWeight.bold,
        color: Color(corTexto),
        fontSize: tamanhoFonte,
      ),
    ),
  );
}

// método de criar uma caixa de texto para preenchimento do usuário
preenchimentoUsuario(String texto, TextInputType tipoTeclado, Icon icone) {
  return Padding(
  padding: EdgeInsets.only(right: 20, left: 20,),
    child: Material(
      borderRadius: BorderRadius.all(Radius.circular(tamanhoRaio),), // raio da borda
      elevation: tamanhoSombra,
      shadowColor: Color(corSombra),
      color: Color(corFundo),
      child: TextField(
          keyboardType: tipoTeclado,
          textAlign: TextAlign.center,
          decoration: new InputDecoration(
            hintText: texto, // Texto da caixa de texto "senha"
            hintStyle: GoogleFonts.libreFranklin(
            textStyle: TextStyle( // estilo do texto que vai ser exibido
              color: Color(corPrimaria),
              fontWeight: FontWeight.w400,
              fontSize: tamanhoLetra,
            ),
          ),
          prefixIcon: icone,
          focusedBorder: OutlineInputBorder( // borda quando esta selecionada
            borderSide: BorderSide(color: Color(corPrimaria)), // Cor da borda
            borderRadius: BorderRadius.all(Radius.circular(tamanhoRaio),), // Raio da borda
          ),
          enabledBorder: OutlineInputBorder( // borda quando nao esta selecionada
            borderSide: BorderSide(color: Color(corPrimaria)), // Cor da borda
            borderRadius: BorderRadius.all(Radius.circular(tamanhoRaio),), // Raio da borda
          ),
        ),
        style: GoogleFonts.getFont(
          fonte,
          fontSize: tamanhoLetra,
          color: Color(corPrimaria),
        ),
      ),
    ),
  );
}

// método de criar linhas na tela
divisao(int corLinha, double alturaLinha, double grossuraLinha, double indentacaoLinha) {
  return Container(
    child: Divider(
      color: Color(corLinha), // cor da linha
      height: alturaLinha, // altura da linha
      thickness: grossuraLinha, // grossura da linha
      indent: indentacaoLinha, // identação no começo
      endIndent: indentacaoLinha, // identação no final
    ),
  );
}

// método de criar botao com imagem e texto
botaoComImagem(double raio, double altura, double largura, double opacidade, String texto, int corTexto, int corSombraTexto, String caminhoImagem, BuildContext context, String pagina) {
  return Material(
    borderRadius: BorderRadius.all(Radius.circular(raio)),
    elevation: tamanhoSombra,
    shadowColor: Color(corSombra),
    color: Colors.transparent,
    child: InkWell(
      borderRadius: BorderRadius.all(Radius.circular(raio)),
      splashColor: Color(corSecundaria).withOpacity(opacidade),
      child: Ink(
        height: altura,
        width: largura,
        child: Center(
          child: Text(
            texto,
            textAlign: TextAlign.center,
            style: GoogleFonts.getFont(
              fonte,
              fontWeight: FontWeight.bold,
              color: Color(corTexto),
              fontSize: 20,
              shadows: <Shadow>[
                Shadow(
                  blurRadius: 10.0,
                  color: Color(corSombraTexto),
                ),
                Shadow(
                  blurRadius: 10.0,
                  color: Color(corSombraTexto),
                ),
                Shadow(
                  blurRadius: 10.0,
                  color: Color(corSombraTexto),
                ),
                Shadow(
                  blurRadius: 10.0,
                  color: Color(corSombraTexto),
                ),
                Shadow(
                  blurRadius: 10.0,
                  color: Color(corSombraTexto),
                ),
              ],
            ),
          ),
        ),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(20)),
          image: DecorationImage(
            image: AssetImage(caminhoImagem),
            fit: BoxFit.cover,
          ),
        ),
      ),
      onLongPress: () {
        Navigator.pushNamed(context, pagina);
      },
    ),
  );
}

// método de criar um botao com estado
botaoCorEstado(String pagina, String texto, BuildContext context, String imagem) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Material(
          borderRadius: BorderRadius.all(Radius.circular(50),), // raio da borda
          elevation: tamanhoSombra,
          shadowColor: Color(corSombra),
          color: Colors.transparent,
          child: GestureDetector(
            child: CircleAvatar(
              radius: 50,
              backgroundColor: Color(corSuporteFundo),
              child: CircleAvatar(
                radius: 45,
                backgroundColor: Color(corFundo),
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                    image: DecorationImage(
                      image: AssetImage(imagem),
                    ),
                  ),
                ),
              ),
            ),
            onTap: () => Navigator.pushNamed(context, pagina), // OnPressed genérico
          ),
        ),
        pularLinha(10),
        caixaDeTexto(texto, corPrimaria, tamanhoLetra, TextAlign.center),
      ],
    );
  }



