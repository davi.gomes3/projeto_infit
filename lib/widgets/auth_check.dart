import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:testeteladelogin/View/telaLogin.dart';
import 'package:testeteladelogin/View/telaPrincipal.dart';
import 'package:testeteladelogin/services/auth_service.dart';

class AuthCheck extends StatefulWidget {
  const AuthCheck({ Key? key }) : super(key: key);

  @override
  _AuthCheckState createState() => _AuthCheckState();
}

class _AuthCheckState extends State<AuthCheck> {
  @override
  Widget build(BuildContext context) {

    AuthService auth = Provider.of<AuthService>(context);

    if (auth.usuario == null) return InFITLogin(); // caso o usuário nao estiver logado ir para tela de login
    else return Menu(); // caso o contrário o usuário ja vai estar logado e so deve ir para a tela inicial

  }
}