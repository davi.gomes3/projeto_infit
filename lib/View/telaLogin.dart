import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:testeteladelogin/Model/metodosTela.dart';
import 'package:testeteladelogin/services/auth_service.dart';

class InFITLogin extends StatefulWidget {
  const InFITLogin({ Key? key }) : super(key: key);

  @override
  _InFITLoginState createState() => _InFITLoginState();
}

class _InFITLoginState extends State<InFITLogin> {

  final formKey = GlobalKey<FormState>();
  final email = TextEditingController();
  final senha = TextEditingController();

  bool estaLogado = true;
  late String titulo; // tema da pagina
  late String botao1; // botao de concluir
  late String botao2; // botao de revogar

  @override
  void initState() {
    super.initState();
    setFormAction(true);
  }

  setFormAction(bool acionar) {
    setState(() {
      estaLogado = acionar;
      if (!estaLogado) { // caso tenha conta
        Navigator.pushNamed(context, "cadastro");
      }
    });
  }

  // método logar (deve ser async)
  logar() async {
    try {
      await context.read<AuthService>().logar(email.text, senha.text);
    } 
    on AutenticarException catch (e) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(e.mensagem)));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(corFundo),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.only(top: 180, right: 40, left: 40,),
          child: Form(
            key: formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                caixaDeTexto("Login - InFIT", corPrimaria, 30, TextAlign.center), pularLinha(40),               
                TextFormField(
                  controller: email,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: "Email",
                  ),
                  keyboardType: TextInputType.emailAddress,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return "Informe o email corretamente";
                    }
                    return null;
                  },
                ), pularLinha(20),
                TextFormField(
                  controller: senha,
                  obscureText: true,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: "Senha",
                  ),
                  keyboardType: TextInputType.emailAddress,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return "Informe sua senha";
                    }
                    else if (value.length < 6) {
                      return "Sua senha deve possuir no mínimo 6 caracteres";
                    }
                    return null;
                  },
                ), pularLinha(20),
                ElevatedButton(
                  onPressed: () {
                    if (formKey.currentState!.validate()) {
                      logar();
                    }
                  },
                  child: Text("Entrar"),
                ), pularLinha(20),
                botaoSemBorda("Esqueceu a senha?", "recuperacao", corSecundaria, context), pularLinha(20),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    caixaDeTexto("Não possui conta?", corPrimaria, tamanhoLetra, TextAlign.center),
                    botaoSemBorda("Registre-se", "cadastro", corPrimaria, context)
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}






/*class InfitLogin extends StatefulWidget {
  @override
  _InfitLoginState createState() => _InfitLoginState();
}

class _InfitLoginState extends State<InfitLogin> {
  GlobalKey<ScaffoldState> _scaffoldState = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldState,
      backgroundColor: Color(corFundo),
      drawer: ClipRRect(
        borderRadius: BorderRadius.horizontal(right: Radius.circular(20)),
        child: Drawer(
          child: Container(
            color: Color(corSuporteFundo),
            padding: EdgeInsets.only(top: 20, right: 40, left: 40,),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                DrawerHeader(
                  child: Material(
                    borderRadius: BorderRadius.all(Radius.circular(10),),
                    elevation: tamanhoSombra,
                    shadowColor: Color(corSuporteFundo),
                    color: Color(corFundo),
                    child: Container(
                      padding: const EdgeInsets.only(top: 20, left: 70, right: 70, bottom: 100),
                      decoration: BoxDecoration(
                        color: Color(corFundo),
                        borderRadius: BorderRadius.all(Radius.circular(10),),
                      ),
                      child: Container(
                        decoration: BoxDecoration(
                          color: Color(corPrimaria),
                          borderRadius: BorderRadius.all(Radius.circular(10),),
                        ),
                      ),
                    ),
                  ),
                ),
                caixaDeTexto("Preview", corSecundaria, 20, TextAlign.center), pularLinha(25),
                divisao(corPrimaria, 10, 1, 5), pularLinha(10),
                caixaDeTexto("Escolha o tema", corSecundaria, tamanhoLetra, TextAlign.center), pularLinha(10),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    botaoCorEstadoGradiente(0xFF444444, 0xFF333333, TextAlign.center, 2),
                    botaoCorEstadoGradiente(0xFFFFFFFF, 0xFFFFFFFF, TextAlign.center, 2),
                  ],
                ), pularLinha(10),
                divisao(corPrimaria, 10, 1, 5), pularLinha(10),
                caixaDeTexto("Escolha a paleta de cores", corSecundaria, tamanhoLetra, TextAlign.center), pularLinha(10),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    botaoCorEstadoGradiente(0xFF32CBFF, 0xFF00A5E0, TextAlign.center, 1),
                    botaoCorEstadoGradiente(0xFFF37748, 0xFFD56062, TextAlign.center, 1),
                    botaoCorEstadoGradiente(0xFF9ECE9A, 0xFF74A57F, TextAlign.center, 1),
                  ],
                ), pularLinha(10),
                divisao(corPrimaria, 10, 1, 5), pularLinha(10),
                caixaDeTexto("Modo daltonismo", corSecundaria, tamanhoLetra, TextAlign.center), pularLinha(10),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    botaoCorEstadoGradiente(0xFF80BDD9, 0xFF877FD7, TextAlign.center, 1),
                    botaoCorEstadoGradiente(0xFFFFD4B8, 0xFFFEB7B3, TextAlign.center, 1),
                    botaoCorEstadoGradiente(0xFF97E5D7, 0xFFD2EBD8, TextAlign.center, 1),
                  ],
                ), pularLinha(20),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    caixaDeTexto("deuteranopia", corPrimaria, tamanhoLetra, TextAlign.center),
                    caixaDeTexto("protanopia", corPrimaria, tamanhoLetra, TextAlign.center),
                    caixaDeTexto("tritanopia", corPrimaria, tamanhoLetra, TextAlign.center),
                  ],
                ), pularLinha(35),
                Container(
                  child: TextButton(
                    child: Text(
                      "Sair", // texto que sera exibido
                      style: GoogleFonts.getFont(
                        fonte,
                        fontWeight: FontWeight.bold,
                        color: Color(corSecundaria),
                        fontSize: tamanhoLetra,
                      ),
                    ),
                    onPressed: () => SystemNavigator.pop(), // OnPressed genérico
                  ),
                ), 
              ],
            ),
          ),
        ),
      ),
      body: Container(
        padding: EdgeInsets.only(top: 20, right: 40, left: 40,),
        child: Container(
          child: ListView(
            children: <Widget>[
              Container(
                alignment: Alignment.topLeft,
                child: CircleAvatar(
                  radius: 20,
                  backgroundColor: Color(corPrimaria),
                  child: GestureDetector(
                    child: Icon(Icons.settings, color: Color(corFundo)),
                    onTap: () {
                      _scaffoldState.currentState!.openDrawer();
                    },
                  ),
                ),
              ),
              //botaoDrawer(context), 
              //Container(
              /*alignment: Alignment.topLeft,
                child: CircleAvatar(
                  radius: 20,
                  backgroundColor: Color(corPrimaria),
                    child: GestureDetector(
                      child: Icon(Icons.settings, color: Color(corFundo)),
                      onTap: () {
                        Scaffold.of(context).openDrawer();
                      },
                    ),
                  ),
              ),*/
              pularLinha(40),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  caixaDeTexto("Login - InFit", corPrimaria, 30, TextAlign.center), pularLinha(120),
                  preenchimentoUsuario("E-mail ou nome de usuário", TextInputType.emailAddress, Icon(Icons.people, color: Color(corPrimaria),)), pularLinha(20),
                  preenchimentoUsuario("Senha", TextInputType.text, Icon(Icons.lock, color: Color(corPrimaria),)), pularLinha(50),
                  botaoComBorda("    Entrar    ", 'menu', corSecundaria, 0xFFFFFFFF, context), pularLinha(30),
                  botaoComBorda("    EntrarTeste    ", 'animado', corSecundaria, 0xFFFFFFFF, context), pularLinha(30),
                  botaoSemBorda("Esqueceu a senha?", 'recuperacao', corPrimaria, context), pularLinha(30),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      caixaDeTexto("Não possui conta?", corPrimaria, tamanhoLetra, TextAlign.center),
                      botaoSemBorda("Registre-se", 'cadastro', corSecundaria, context),
                    ],
                  ),
                ],
              ),
            ],
          ),
        )
      )
    );
  }

  botaoCorEstadoGradiente(int cor, int cor2, TextAlign ajustar, double numero) {
    return Material(
      borderRadius: BorderRadius.all(Radius.circular(30),), // raio da borda
      elevation: tamanhoSombra,
      shadowColor: Color(corSombra),
      color: Colors.transparent,
      child: Container(
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          gradient: LinearGradient(colors: [
            Color(cor),
            Color(cor2),
          ]),
        ),
        child: CircleAvatar(
          radius: 30,
          backgroundColor: Colors.transparent,
          child: GestureDetector(
            onTap: () { // ontap quando for pressionado o botao
              setState(() { // setstate() para setar o estado
                if (numero == 1) {
                  corPrimaria = cor;
                  corSecundaria = cor2;
                }
                else {
                  corFundo = cor;
                  corSuporteFundo = cor2;
                }
              });
            },
          ),
        ),
      ),
    );
  }
  
}*/