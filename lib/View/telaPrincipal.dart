import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:testeteladelogin/Model/metodosTela.dart';
import 'package:testeteladelogin/services/auth_service.dart';

class Menu extends StatefulWidget {
  /*String email = "";
  String nome = "";
  double altura = 0;
  double peso = 0;
  String senha = "";
  //Date datanascimento;

  Menu() {
    this.email = email;
    this.nome = nome;
    this.altura = altura;
    this.peso = peso;
    this.senha = senha;
  }

  Menu.withA([email, nome, altura, peso, senha]) {
    this.email = email;
    this.nome = nome;
    this.altura = altura;
    this.peso = peso;
    this.senha = senha;
  }*/

  @override
  _MenuState createState() => _MenuState();
}

class _MenuState extends State<Menu> {
  GlobalKey<ScaffoldState> _scaffoldState = GlobalKey<ScaffoldState>();

  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldState,
      backgroundColor: Color(corFundo),
      drawer: ClipRRect(
        borderRadius: BorderRadius.horizontal(right: Radius.circular(20)),
        child: Drawer(
          child: Container(
            color: Color(corFundo),
            padding: EdgeInsets.only(
              top: 20,
              right: 40,
              left: 40,
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                pularLinha(120),
                Material(
                  borderRadius: BorderRadius.all(
                    Radius.circular(80),
                  ), // raio da borda
                  elevation: tamanhoSombra,
                  shadowColor: Color(corSombra),
                  color: Colors.transparent,
                  child: GestureDetector(
                    child: CircleAvatar(
                      radius: 80,
                      backgroundColor: Color(corSecundaria),
                      child: CircleAvatar(
                        radius: 70,
                        backgroundColor: Color(corFundo),
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(20)),
                            image: DecorationImage(
                              image: AssetImage(
                                  'assets/imagens/gerais/laranja.png'),
                            ),
                          ),
                        ),
                      ),
                    ),
                    onTap: () {
                      // Navigator.pushNamed(context, ExerciciosPage()); // OnPressed genérico
                    },
                  ),
                ),
                pularLinha(40),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        CircleAvatar(
                          backgroundColor: Color(corPrimaria),
                        ),
                        Container(
                          child: TextButton(
                            child: Text(
                              'PERFIL', // texto que sera exibido
                              style: GoogleFonts.getFont(
                                fonte,
                                fontWeight: FontWeight.bold,
                                color: Color(corPrimaria),
                                fontSize: tamanhoLetra,
                              ),
                            ),
                            onPressed: () {
                              Navigator.pushNamed(context, "perfil");
                            },
                          ),
                        ),
                      ],
                    ),
                    pularLinha(20),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        CircleAvatar(
                          backgroundColor: Color(corPrimaria),
                        ),
                        botaoSemBorda(
                            'ALTERAR DADOS', "", corPrimaria, context),
                      ],
                    ),
                    pularLinha(20),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        CircleAvatar(
                          backgroundColor: Color(corPrimaria),
                        ),
                        Container(
                          child: TextButton(
                            child: Text(
                              'PERSONALIZAR TEMAS', // texto que sera exibido
                              style: GoogleFonts.getFont(
                                fonte,
                                fontWeight: FontWeight.bold,
                                color: Color(corPrimaria),
                                fontSize: tamanhoLetra,
                              ),
                            ),
                            onPressed: () {}, // OnPressed genérico
                          ),
                        ),
                      ],
                    ),
                    pularLinha(40),
                    Container(
                      child: TextButton(
                        child: Text(
                          "Sair", // texto que sera exibido
                          style: GoogleFonts.getFont(
                            fonte,
                            fontWeight: FontWeight.bold,
                            color: Color(corSecundaria),
                            fontSize: tamanhoLetra,
                          ),
                        ),
                        onPressed: () {
                          showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return AlertDialog(
                                  title: caixaDeTexto(
                                      "Deseja mesmo sair ou somente encerrar sua sessão?",
                                      corPrimaria,
                                      18,
                                      TextAlign.center),
                                  actions: <Widget>[
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      children: [
                                        Container(
                                          child: TextButton(
                                            child: Text(
                                              "Sair", // texto que sera exibido
                                              style: GoogleFonts.getFont(
                                                fonte,
                                                fontWeight: FontWeight.bold,
                                                color: Color(corSecundaria),
                                                fontSize: tamanhoLetra,
                                              ),
                                            ),
                                            onPressed: () => SystemNavigator
                                                .pop(), // OnPressed genérico
                                          ),
                                        ),
                                        Container(
                                          child: TextButton(
                                            child: Text(
                                              "Encerrar Sessão", // texto que sera exibido
                                              style: GoogleFonts.getFont(
                                                fonte,
                                                fontWeight: FontWeight.bold,
                                                color: Color(corSecundaria),
                                                fontSize: tamanhoLetra,
                                              ),
                                            ),
                                            onPressed: () {
                                              Navigator.pop(context);
                                              context
                                                  .read<AuthService>()
                                                  .sair();
                                            }, // OnPressed genérico
                                          ),
                                        ),
                                      ],
                                    ),
                                  ]);
                            },
                          );
                        },
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
      body: Container(
        padding: EdgeInsets.only(
          top: 20,
          right: 40,
          left: 40,
        ),
        child: ListView(
          children: <Widget>[
            Container(
              alignment: Alignment.topLeft,
              child: CircleAvatar(
                radius: 20,
                backgroundColor: Color(corPrimaria),
                child: GestureDetector(
                  child: Icon(Icons.reorder, color: Color(corFundo)),
                  onTap: () {
                    _scaffoldState.currentState!.openDrawer();
                  },
                ),
              ),
            ),
            pularLinha(40),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    botaoCorEstado("alimentacao", "Alimentos", context,
                        'assets/imagens/menu/alimentos.png'),
                    botaoCorEstado("exercicios", "Exercícios", context,
                        'assets/imagens/menu/exercicio.png'),
                  ],
                ),
                pularLinha(30),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
