import 'package:flutter/material.dart';
import 'package:testeteladelogin/Model/metodosTela.dart';

class TelaTeste extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(corFundo),
      body:  Container(
        padding: EdgeInsets.only(top: 20, right: 40, left: 40,),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            pularLinha(20),
            caixaDeTexto("Alimentos", corPrimaria, 30, TextAlign.center), pularLinha(20),
            caixaDeTexto('O que você deseja cozinhar hoje?', corPrimaria, 15, TextAlign.center), pularLinha(40),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                botaoCorEstado('login', 'Salgado', context, 'assets/imagens/teste/salgado.png'),
                botaoCorEstado('login', 'Doce', context, 'assets/imagens/teste/doce.png'),
              ],
            ), pularLinha(20),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                botaoCorEstado('login', 'Bebidas', context, 'assets/imagens/teste/bebida.png'),
                botaoCorEstado('login', 'Vegano', context, 'assets/imagens/teste/vegano.png'),
              ],
            ), pularLinha(20),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                botaoCorEstado('login', 'Com frutas', context, 'assets/imagens/gerais/laranja.png'),
                botaoCorEstado('login', 'Low carb', context, 'assets/imagens/teste/lowcarb.png'),
              ],
            ), pularLinha(40),
            botaoSemBorda("Retornar", 'menu', corSecundaria, context),
          ],
        ),
      ),
    );
  }
}