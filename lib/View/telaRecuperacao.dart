import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:testeteladelogin/Model/metodosTela.dart';
import 'package:testeteladelogin/services/auth_service.dart';

class Recuperacao extends StatefulWidget {
  const Recuperacao({ Key? key }) : super(key: key);
  
  @override
  _RecuperacaoState createState() => _RecuperacaoState();
}

class _RecuperacaoState extends State<Recuperacao> {

  //final formKey = GlobalKey<FormState>();
  final email = TextEditingController();
  final auth = FirebaseAuth.instance;

  enviarRecuperacao() async {
    try {
      await context.read<AuthService>().recuperar(email.text);
    } 
    on AutenticarException catch (e) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(e.mensagem)));
    }    
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(corFundo), // cor de fundo da tela
      body: Container( // corpo é um container e terá as limitações de margem do padding
        padding: EdgeInsets.only(top: 60, right: 40, left: 40,), // Padding para criar as margens na tela
        child: ListView(
          //key: formKey,
          children: [
            CircleAvatar( // circulo de terá o icone de cadeado representando a segurança de trocar a senha
              radius: 100,
              child: Icon(Icons.privacy_tip, size: 80,), // adicionando icone
              foregroundColor: Color(corFundo),
              backgroundColor: Color(corSecundaria),
            ),
            pularLinha(20),
            Container(
              child: Column( // coluna para organizar as caixas de texto mais facilmente
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  caixaDeTexto("Esqueceu sua senha?", corPrimaria, 2 * tamanhoLetra, TextAlign.center), // texto de esqueceu sua senha
                  pularLinha(20),
                  caixaDeTexto("Insira o seu endereço de E-mail para ser enviado um link para restaurar suas informações.", corPrimaria, tamanhoLetra, TextAlign.center), // texto de restauração de senha
                  pularLinha(20),
                  TextFormField(
                    controller: email,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: "Insira o email da conta",
                    ),
                    keyboardType: TextInputType.emailAddress,
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return "Informe o email";
                      }
                      return null;
                    },
                  ), pularLinha(20),
                  ElevatedButton(
                    child: Text("Enviar"),
                    onPressed: () {
                      //if (formKey.currentState!.validate()) {
                        enviarRecuperacao();
                        
                      //}
                    },
                  ), pularLinha(30),
                  botaoSemBorda("Retornar", 'login', corSecundaria, context), // botao com o texto "Retornar" e vai ser direcionado para a pagina de login
                ]
              ),
            ),
          ],
        ),
      ),
    );
  }
}