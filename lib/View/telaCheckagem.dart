import 'package:flutter/material.dart';
import 'package:testeteladelogin/Telas/Alongamentos/along1.dart';
import 'package:testeteladelogin/Telas/Alongamentos/along2.dart';
import 'package:testeteladelogin/Telas/Alongamentos/along3.dart';
import 'package:testeteladelogin/Telas/Alongamentos/along4.dart';
import 'package:testeteladelogin/Telas/Alongamentos/along5.dart';
import 'package:testeteladelogin/Telas/Alongamentos/along6.dart';
import 'package:testeteladelogin/Telas/Alongamentos/listaAlong.dart';
import 'package:testeteladelogin/Telas/ExercicioAbdomen/abdomen1.dart';
import 'package:testeteladelogin/Telas/ExercicioAbdomen/abdomen2.dart';
import 'package:testeteladelogin/Telas/ExercicioAbdomen/abdomen3.dart';
import 'package:testeteladelogin/Telas/ExercicioAbdomen/abdomen4.dart';
import 'package:testeteladelogin/Telas/ExercicioAbdomen/abdomen5.dart';
import 'package:testeteladelogin/Telas/ExercicioAbdomen/abdomen6.dart';
import 'package:testeteladelogin/Telas/ExercicioAbdomen/listaAbdomen.dart';
import 'package:testeteladelogin/Telas/Perfil/editarperfil.dart';
import 'package:testeteladelogin/Telas/Perfil/perfil.dart';
import 'package:testeteladelogin/Telas/telaExercicios.dart';
import 'package:testeteladelogin/Telas/ExercicioBra%C3%A7o/braco1.dart';
import 'package:testeteladelogin/Telas/ExercicioBra%C3%A7o/braco2.dart';
import 'package:testeteladelogin/Telas/ExercicioBra%C3%A7o/braco3.dart';
import 'package:testeteladelogin/Telas/ExercicioBra%C3%A7o/braco4.dart';
import 'package:testeteladelogin/Telas/ExercicioBra%C3%A7o/braco5.dart';
import 'package:testeteladelogin/Telas/ExercicioBra%C3%A7o/braco6.dart';
import 'package:testeteladelogin/Telas/ExercicioBra%C3%A7o/listaBraco.dart';
import 'package:testeteladelogin/Telas/ExerciciosCosta/costa1.dart';
import 'package:testeteladelogin/Telas/ExerciciosCosta/costa2.dart';
import 'package:testeteladelogin/Telas/ExerciciosCosta/costa3.dart';
import 'package:testeteladelogin/Telas/ExerciciosCosta/costa4.dart';
import 'package:testeteladelogin/Telas/ExerciciosCosta/costa5.dart';
import 'package:testeteladelogin/Telas/ExerciciosCosta/costa6.dart';
import 'package:testeteladelogin/Telas/ExerciciosCosta/listaCosta.dart';
import 'package:testeteladelogin/Telas/ExerciciosPeito/listaPeito.dart';
import 'package:testeteladelogin/Telas/ExerciciosPeito/peito1.dart';
import 'package:testeteladelogin/Telas/ExerciciosPeito/peito2.dart';
import 'package:testeteladelogin/Telas/ExerciciosPeito/peito3.dart';
import 'package:testeteladelogin/Telas/ExerciciosPeito/peito4.dart';
import 'package:testeteladelogin/Telas/ExerciciosPeito/peito5.dart';
import 'package:testeteladelogin/Telas/ExerciciosPeito/peito6.dart';
import 'package:testeteladelogin/Telas/ExerciciosPerna/listaPerna.dart';
import 'package:testeteladelogin/Telas/ExerciciosPerna/perna1.dart';
import 'package:testeteladelogin/Telas/ExerciciosPerna/perna2.dart';
import 'package:testeteladelogin/Telas/ExerciciosPerna/perna3.dart';
import 'package:testeteladelogin/Telas/ExerciciosPerna/perna4.dart';
import 'package:testeteladelogin/Telas/ExerciciosPerna/perna5.dart';
import 'package:testeteladelogin/Telas/ExerciciosPerna/perna6.dart';
import 'package:testeteladelogin/View/telaAlimentos.dart';
import 'package:testeteladelogin/View/telaCadastro.dart';
import 'package:testeteladelogin/View/telaLogin.dart';
import 'package:testeteladelogin/View/telaPrincipal.dart';
import 'package:testeteladelogin/View/telaRecuperacao.dart';
import 'package:testeteladelogin/widgets/auth_check.dart';

import 'package:testeteladelogin/Telas/ComFrutas.dart';
import 'package:testeteladelogin/Telas/alimentacao.dart';
import 'package:testeteladelogin/Telas/MenuAlimentos.dart';
import 'package:testeteladelogin/Telas/TelaNutricionistas.dart';
// FRUTAS BANANA
import 'package:testeteladelogin/Telas/AlimentacaoComFrutasBanana/comFrutasBanana.dart';
import 'package:testeteladelogin/Telas/AlimentacaoComFrutasBanana/ReceitaBananaComGraos.dart';
import 'package:testeteladelogin/Telas/AlimentacaoComFrutasBanana/ReceitaBoloDeBanana.dart';
import 'package:testeteladelogin/Telas/AlimentacaoComFrutasBanana/ReceitaBrigadeiroDeBanana.dart';
import 'package:testeteladelogin/Telas/AlimentacaoComFrutasBanana/ReceitaCookieDeBanana.dart';
import 'package:testeteladelogin/Telas/AlimentacaoComFrutasBanana/ReceitaSorveteDeBanana.dart';
import 'package:testeteladelogin/Telas/AlimentacaoComFrutasBanana/ReceitaPanquecaDeBanana.dart';
// FRUTAS ABACAXI
import 'package:testeteladelogin/Telas/AlimentacaoComFrutasAbacaxi/comFrutasAbacaxi.dart';
import 'package:testeteladelogin/Telas/AlimentacaoComFrutasAbacaxi/SaladaComAbacaxi.dart';
import 'package:testeteladelogin/Telas/AlimentacaoComFrutasAbacaxi/ReceitaEspetinhoDeAbacaxi.dart';
import 'package:testeteladelogin/Telas/AlimentacaoComFrutasAbacaxi/ReceitaMousseDeAbacaxi.dart';
import 'package:testeteladelogin/Telas/AlimentacaoComFrutasAbacaxi/ReceitaSorveteDeAbacaxi.dart';
import 'package:testeteladelogin/Telas/AlimentacaoComFrutasAbacaxi/ReceitaPeixeComAbacaxi.dart';
import 'package:testeteladelogin/Telas/AlimentacaoComFrutasAbacaxi/ReceitaBoloDeAbacaxi.dart';
// FRUTAS ABACATE
import 'package:testeteladelogin/Telas/AlimentacaoComFrutasAbacate/comFrutasAbacate.dart';
import 'package:testeteladelogin/Telas/AlimentacaoComFrutasAbacate/ReceitaGuacamoleFit.dart';
import 'package:testeteladelogin/Telas/AlimentacaoComFrutasAbacate/ReceitaSaladaComAbacate.dart';
import 'package:testeteladelogin/Telas/AlimentacaoComFrutasAbacate/ReceitaMousseDeAbacate.dart';
import 'package:testeteladelogin/Telas/AlimentacaoComFrutasAbacate/ReceitaAbacateRecheado.dart';
import 'package:testeteladelogin/Telas/AlimentacaoComFrutasAbacate/ReceitaBrigadeiroDeAbacate.dart';
import 'package:testeteladelogin/Telas/AlimentacaoComFrutasAbacate/ReceitaSorveteDeAbacate.dart';
// FRUTAS MORANGO
import 'package:testeteladelogin/Telas/AlimentacaoComFrutasMorango/comFrutasMorango.dart';
import 'package:testeteladelogin/Telas/AlimentacaoComFrutasMorango/ReceitaSmoothieDeMorango.dart';
import 'package:testeteladelogin/Telas/AlimentacaoComFrutasMorango/ReceitaPanquecaComMorango.dart';
import 'package:testeteladelogin/Telas/AlimentacaoComFrutasMorango/ReceitaSaladaComMorango.dart';
import 'package:testeteladelogin/Telas/AlimentacaoComFrutasMorango/ReceitaBeijinhoDeMorango.dart';
import 'package:testeteladelogin/Telas/AlimentacaoComFrutasMorango/ReceitaGeleiaDeMorango.dart';
import 'package:testeteladelogin/Telas/AlimentacaoComFrutasMorango/ReceitaCremeDeMorango.dart';
// FRUTAS KIWI
import 'package:testeteladelogin/Telas/AlimentacaoComFrutasKiwi/comFrutasKiwi.dart';
import 'package:testeteladelogin/Telas/AlimentacaoComFrutasKiwi/ReceitaLimonadaDeKiwi.dart';
import 'package:testeteladelogin/Telas/AlimentacaoComFrutasKiwi/ReceitaPanquecaComKiwi.dart';
import 'package:testeteladelogin/Telas/AlimentacaoComFrutasKiwi/ReceitaGeleiaDeKiwi.dart';
import 'package:testeteladelogin/Telas/AlimentacaoComFrutasKiwi/ReceitaSucoRefrescanteKiwi.dart';
import 'package:testeteladelogin/Telas/AlimentacaoComFrutasKiwi/ReceitaPicoleDeKiwi.dart';
import 'package:testeteladelogin/Telas/AlimentacaoComFrutasKiwi/ReceitaCaipirinhaDeKiwi.dart';
// FRUTAS MELANCIA
import 'package:testeteladelogin/Telas/AlimentacaoComFrutasMelancia/comFrutasMelancia.dart';
import 'package:testeteladelogin/Telas/AlimentacaoComFrutasMelancia/ReceitaSucoDeMelancia.dart';
import 'package:testeteladelogin/Telas/AlimentacaoComFrutasMelancia/ReceitaGranitaRosa.dart';
import 'package:testeteladelogin/Telas/AlimentacaoComFrutasMelancia/ReceitaMolhoDeMelancia.dart';
import 'package:testeteladelogin/Telas/AlimentacaoComFrutasMelancia/ReceitaPicoleDeMelancia.dart';
import 'package:testeteladelogin/Telas/AlimentacaoComFrutasMelancia/ReceitaSaladaComMelancia.dart';
import 'package:testeteladelogin/Telas/AlimentacaoComFrutasMelancia/ReceitaGeleiaDeMelancia.dart';
// TELAS BEBIDAS
import 'package:testeteladelogin/Telas/AlimentacaoBebidas/bebidas.dart';
import 'package:testeteladelogin/Telas/AlimentacaoBebidas/ReceitaVitaminaMangaEBanana.dart';
import 'package:testeteladelogin/Telas/AlimentacaoBebidas/ReceitaVitaminaLaranja.dart';
import 'package:testeteladelogin/Telas/AlimentacaoBebidas/ReceitaVitaminaAbacateEHortela.dart';
import 'package:testeteladelogin/Telas/AlimentacaoBebidas/ReceitaVitaminaBananaMorangoAveia.dart';
import 'package:testeteladelogin/Telas/AlimentacaoBebidas/ReceitaVitaminaBananaEAmendoim.dart';
import 'package:testeteladelogin/Telas/AlimentacaoBebidas/ReceitaVitaminaRoxa.dart';
// TELAS LOW CARB
import 'package:testeteladelogin/Telas/AlimentacaoLowCarb/lowCarb.dart';
import 'package:testeteladelogin/Telas/AlimentacaoLowCarb/ReceitaEspaguete.dart';
import 'package:testeteladelogin/Telas/AlimentacaoLowCarb/ReceitaFarofa.dart';
import 'package:testeteladelogin/Telas/AlimentacaoLowCarb/ReceitaPanacota.dart';
import 'package:testeteladelogin/Telas/AlimentacaoLowCarb/ReceitaQuiche.dart';
import 'package:testeteladelogin/Telas/AlimentacaoLowCarb/ReceitaRisoto.dart';
import 'package:testeteladelogin/Telas/AlimentacaoLowCarb/ReceitaWrapDeCouve.dart';
// TELAS VEGANAS
import 'package:testeteladelogin/Telas/AlimentacaoVeganas/veganas.dart';
import 'package:testeteladelogin/Telas/AlimentacaoVeganas/ReceitaBoloDeChocolateVegano.dart';
import 'package:testeteladelogin/Telas/AlimentacaoVeganas/ReceitaCheddarVegano.dart';
import 'package:testeteladelogin/Telas/AlimentacaoVeganas/ReceitaEspetinhoVegano.dart';
import 'package:testeteladelogin/Telas/AlimentacaoVeganas/ReceitaRatatouilleVegano.dart';
import 'package:testeteladelogin/Telas/AlimentacaoVeganas/ReceitaToffeDeCenouraEMaca.dart';
import 'package:testeteladelogin/Telas/AlimentacaoVeganas/ReceitaHamburguerVegano.dart';
// TELAS DOCES
import 'package:testeteladelogin/Telas/AlimentacaoDoces/doces.dart';
import 'package:testeteladelogin/Telas/AlimentacaoDoces/ReceitaDocinhoDeCoco.dart';
import 'package:testeteladelogin/Telas/AlimentacaoDoces/ReceitaTamaraECoco.dart';
import 'package:testeteladelogin/Telas/AlimentacaoDoces/ReceitaBrownieFit.dart';
import 'package:testeteladelogin/Telas/AlimentacaoDoces/ReceitaPaoDeMel.dart';
import 'package:testeteladelogin/Telas/AlimentacaoDoces/ReceitaPudimDeChia.dart';
import 'package:testeteladelogin/Telas/AlimentacaoDoces/ReceitaPudimDeLeite.dart';
// TELAS SALGADOS
import 'package:testeteladelogin/Telas/AlimentacaoSalgada/salgadas.dart';
import 'package:testeteladelogin/Telas/AlimentacaoSalgada/ReceitaPaoDeQueijo.dart';
import 'package:testeteladelogin/Telas/AlimentacaoSalgada/ReceitaMacarraoAMilanesa.dart';
import 'package:testeteladelogin/Telas/AlimentacaoSalgada/ReceitaPizzaDeFrigideira.dart';
import 'package:testeteladelogin/Telas/AlimentacaoSalgada/ReceitaSalada.dart';
import 'package:testeteladelogin/Telas/AlimentacaoSalgada/ReceitaFricasse.dart';
import 'package:testeteladelogin/Telas/AlimentacaoSalgada/ReceitaHamburguer.dart';

class InFIT extends StatelessWidget {
  const InFIT({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "InFIT",
      debugShowCheckedModeBanner: false, // desabilitar a marca de debug
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ), // tema da cor
      home: AuthCheck(), // homepage (primeira pagina do aplicativo)
      routes: <String, WidgetBuilder>{
        'login': (BuildContext context) => InFITLogin(),
        'cadastro': (BuildContext context) => InFITCadastro(),
        'menu': (BuildContext context) => Menu(),
        'recuperacao': (BuildContext context) => Recuperacao(),
        'teste': (BuildContext context) => TelaTeste(),
        'perfil': (context) => ProfilePage(),
        'editarPerfil': (context) => EditProfilePage(),

        // ROTAS EXERCICIO
        'telaBraco': (context) => ListaBraco(),
        'telaAbd': (context) => ListaAbdomen(),
        'telaCosta': (context) => ListaCosta(),
        'telaPeito': (context) => ListaPeito(),
        'telaPerna': (context) => ListaPerna(),
        'telaAlong': (context) => ListaAlong(),

        'along1': (context) => AlongamentoUm(),
        'along2': (context) => AlongamentoDois(),
        'along3': (context) => AlongamentoTres(),
        'along4': (context) => AlongamentoQuatro(),
        'along5': (context) => AlongamentoCinco(),
        'along6': (context) => AlongamentoSeis(),

        'perna1': (context) => PernaUm(),
        'perna2': (context) => PernaDois(),
        'perna3': (context) => PernaTres(),
        'perna4': (context) => PernaQuatro(),
        'perna5': (context) => PernaCinco(),
        'perna6': (context) => PernaSeis(),

        'peito1': (context) => PeitoUm(),
        'peito2': (context) => PeitoDois(),
        'peito3': (context) => PeitoTres(),
        'peito4': (context) => PeitoQuatro(),
        'peito5': (context) => PeitoCinco(),
        'peito6': (context) => PeitoSeis(),

        'braco1': (context) => BracoUm(),
        'braco2': (context) => BracoDois(),
        'braco3': (context) => BracoTres(),
        'braco4': (context) => BracoQuatro(),
        'braco5': (context) => BracoCinco(),
        'braco6': (context) => BracoSeis(),

        'abd1': (context) => AbdomenUm(),
        'abd2': (context) => AbdomenDois(),
        'abd3': (context) => AbdomenTres(),
        'abd4': (context) => AbdomenQuatro(),
        'abd5': (context) => AbdomenCinco(),
        'abd6': (context) => AbdomenSeis(),

        'costa1': (context) => CostaUm(),
        'costa2': (context) => CostaDois(),
        'costa3': (context) => CostaTres(),
        'costa4': (context) => CostaQuatro(),
        'costa5': (context) => CostaCinco(),
        'costa6': (context) => CostaSeis(),

        'exercicios': (context) => Teste(),

        // ROTAS DE ALIMENTAÇÃO
        'TelaNutricionistas': (context) => TelaNutricionistas(),
        'MenuAlimentos': (context) => MenuAlimentos(),
        'comFrutas': (context) => TelaComFrutas(),
        'alimentacao': (context) => MeuApp(),
        'comFrutasBanana': (context) => TelaComFrutasBanana(),
        'receitaBananaComGraos': (context) => ReceitaBananaComGraos(),
        'ReceitaBoloDeBanana': (context) => ReceitaBoloDeBanana(),
        'ReceitaBrigadeiroDeBanana': (context) => ReceitaBrigadeiroDeBanana(),
        'ReceitaCookieDeBanana': (context) => ReceitaCookieDeBanana(),
        'ReceitaSorveteDeBanana': (context) => ReceitaSorveteDeBanana(),
        'ReceitaPanquecaDeBanana': (context) => ReceitaPanquecaDeBanana(),
        'salgadas': (context) => TelaSalgadas(),
        'ReceitaPaoDeQueijo': (context) => ReceitaPaoDeQueijo(),
        'ReceitaMacarraoAMilanesa': (context) => ReceitaMacarraoAMilanesa(),
        'ReceitaPizzaDeFrigideira': (context) => ReceitaPizzaDeFrigideira(),
        'ReceitaSalada': (context) => ReceitaSalada(),
        'ReceitaFricasse': (context) => ReceitaFricasse(),
        'ReceitaHamburguer': (context) => ReceitaHamburguer(),
        'TelaComFrutasAbacaxi': (context) => TelaComFrutasAbacaxi(),
        'ReceitaSaladaComAbacaxi': (context) => ReceitaSaladaComAbacaxi(),
        'ReceitaEspetinhoDeAbacaxi': (context) => ReceitaEspetinhoDeAbacaxi(),
        'ReceitaMousseDeAbacaxi': (context) => ReceitaMousseDeAbacaxi(),
        'ReceitaSorveteDeAbacaxi': (context) => ReceitaSorveteDeAbacaxi(),
        'ReceitaPeixeComAbacaxi': (context) => ReceitaPeixeComAbacaxi(),
        'ReceitaBoloDeAbacaxi': (context) => ReceitaBoloDeAbacaxi(),
        'TelaComFrutasAbacate': (context) => TelaComFrutasAbacate(),
        'ReceitaGuacamoleFit': (context) => ReceitaGuacamoleFit(),
        'ReceitaSaladaComAbacate': (context) => ReceitaSaladaComAbacate(),
        'ReceitaMousseDeAbacate': (context) => ReceitaMousseDeAbacate(),
        'ReceitaAbacateRecheado': (context) => ReceitaAbacateRecheado(),
        'ReceitaBrigadeiroDeAbacate': (context) => ReceitaBrigadeiroDeAbacate(),
        'ReceitaSorveteDeAbacate': (context) => ReceitaSorveteDeAbacate(),
        'TelaComFrutasMorango': (context) => TelaComFrutasMorango(),
        'ReceitaSmoothieDeMorango': (context) => ReceitaSmoothieDeMorango(),
        'ReceitaPanquecaComMorango': (context) => ReceitaPanquecaComMorango(),
        'ReceitaSaladaComMorango': (context) => ReceitaSaladaComMorango(),
        'ReceitaBeijinhoDeMorango': (context) => ReceitaBeijinhoDeMorango(),
        'ReceitaGeleiaDeMorango': (context) => ReceitaGeleiaDeMorango(),
        'ReceitaCremeDeMorango': (context) => ReceitaCremeDeMorango(),
        'TelaComFrutasKiwi': (context) => TelaComFrutasKiwi(),
        'ReceitaLimonadaDeKiwi': (context) => ReceitaLimonadaDeKiwi(),
        'ReceitaPanquecaComKiwi': (context) => ReceitaPanquecaComKiwi(),
        'ReceitaGeleiaDeKiwi': (context) => ReceitaGeleiaDeKiwi(),
        'ReceitaSucoRefrescanteKiwi': (context) => ReceitaSucoRefrescanteKiwi(),
        'ReceitaPicoleDeKiwi': (context) => ReceitaPicoleDeKiwi(),
        'ReceitaCaipirinhaDeKiwi': (context) => ReceitaCaipirinhaDeKiwi(),
        'TelaComFrutasMelancia': (context) => TelaComFrutasMelancia(),
        'ReceitaSucoDeMelancia': (context) => ReceitaSucoDeMelancia(),
        'ReceitaGranitaRosa': (context) => ReceitaGranitaRosa(),
        'ReceitaMolhoDeMelancia': (context) => ReceitaMolhoDeMelancia(),
        'ReceitaPicoleDeMelancia': (context) => ReceitaPicoleDeMelancia(),
        'ReceitaSaladaComMelancia': (context) => ReceitaSaladaComMelancia(),
        'ReceitaGeleiaDeMelancia': (context) => ReceitaGeleiaDeMelancia(),
        'Bebidas': (context) => Bebidas(),
        'ReceitaVitaminaMangaEBanana': (context) =>
            ReceitaVitaminaMangaEBanana(),
        'ReceitaVitaminaLaranja': (context) => ReceitaVitaminaLaranja(),
        'ReceitaVitaminaAbacateEHortela': (context) =>
            ReceitaVitaminaAbacateEHortela(),
        'ReceitaVitaminaBananaMorangoAveia': (context) =>
            ReceitaVitaminaBananaMorangoAveia(),
        'ReceitaVitaminaBananaEAmendoim': (context) =>
            ReceitaVitaminaBananaEAmendoim(),
        'ReceitaVitaminaRoxa': (context) => ReceitaVitaminaRoxa(),
        'TelaLowCarb': (context) => TelaLowCarb(),
        'ReceitaEspaguete': (context) => ReceitaEspaguete(),
        'ReceitaFarofa': (context) => ReceitaFarofa(),
        'ReceitaPanacota': (context) => ReceitaPanacota(),
        'ReceitaQuiche': (context) => ReceitaQuiche(),
        'ReceitaRisoto': (context) => ReceitaRisoto(),
        'ReceitaWrapDeCouve': (context) => ReceitaWrapDeCouve(),
        'TelaVeganas': (context) => TelaVeganas(),
        'ReceitaBoloDeChocolateVegano': (context) =>
            ReceitaBoloDeChocolateVegano(),
        'ReceitaCheddarVegano': (context) => ReceitaCheddarVegano(),
        'ReceitaEspetinhoVegano': (context) => ReceitaEspetinhoVegano(),
        'ReceitaRatatouilleVegano': (context) => ReceitaRatatouilleVegano(),
        'ReceitaToffeDeCenouraEMaca': (context) => ReceitaToffeDeCenouraEMaca(),
        'ReceitaHamburguerVegano': (context) => ReceitaHamburguerVegano(),
        'TelaDoces': (context) => TelaDoces(),
        'ReceitaDocinhoDeCoco': (context) => ReceitaDocinhoDeCoco(),
        'ReceitaTamaraECoco': (context) => ReceitaTamaraECoco(),
        'ReceitaBrownieFit': (context) => ReceitaBrownieFit(),
        'ReceitaPaoDeMel': (context) => ReceitaPaoDeMel(),
        'ReceitaPudimDeChia': (context) => ReceitaPudimDeChia(),
        'ReceitaPudimDeLeite': (context) => ReceitaPudimDeLeite(),
      },
    );
  }
}

/*class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'teste InFIT',
      theme: ThemeData(primarySwatch: Colors.blue,),
      home : InfitLogin(),
      initialRoute: 'login', // rota inicial, ou seja, a primeira pagina que deve ser exibida
      routes: {
        // essas string servirao para podermos nos mover entre as paginas com navigator.pushnamed e navigator.pop
        //'login': (context) => InfitLogin(), // 'login' é a string direcionada a pagina
        //'cadastro': (context) => TelaCadastro(), // 'cadastro' é a string direcionada a pagina
        //'recuperacao': (context) => TelaRecuperacao(), // 'recuperacao é a string direcionada a pagina  
        //'alimento': (context) => TelaTeste(),
        //'menu': (context) => Menu(),
        //'codigo': (context) => Codigo(),
      },
    );
  }
}*/